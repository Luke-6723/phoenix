Phoenix
======
Basic commands: `!ph help basic`<br>
Moderation commands: `!ph help moderator`<br>
Administrator commands: `!ph help admin`<br>

Support
======
If you have a problem and don't know what to do, feel free to drop into the support server and ask: [Invite Link](https://discord.gg/34k4Mk2)<br>
To invite the bot use this link: [Invite Link](https://discordapp.com/oauth2/authorize?client_id=343543091980140544&scope=bot&permissions=8)

To see the source code use one of these 2 places:<br>
[Github (Source Code)](https://github.com/Luke-6723/Phoenix)<br>[Gitlab (Souce Code)](https://gitlab.com/Luke-6723/Phoenix)
