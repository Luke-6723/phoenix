from src.database_handler import Database
from tinydb import Query
import discord
from discord import Embed
import settings
import threading
import asyncio
import time
from time import time as get_time
serversdb = Database("server_mutes")
database = Database("database")
modlogs = Database("modlogs")


class Unmute_Checks():

    def __init__(self, client):
        self.client = client

    async def unmute_user(self, user_id, server_id):
        user = self.client.get_guild(int(server_id)).get_member(int(user_id))
        guild = self.client.get_guild(int(server_id))
        _server = serversdb.get_server(server_id)
        try:
            if _server.get(Query().mutes) != None:
                old_mutes = _server.get(Query().mutes)['mutes']
                new_mutes = []
                for mute in old_mutes:
                    if mute.get(user_id, 0) != 0:
                        new_mutes = old_mutes.remove(mute)
                _server.update({'mutes': new_mutes}, Query().mutes)
        except Exception as e:
            print(e)

        if user == None:
            mod_log_db = modlogs.get_server(guild.id)
            case_number = len(mod_log_db.all()) + 1
            mod_log_db.insert({
                "case": case_number,
                "reason": "Time's up",
                "userid": user.id,
                "operation": "unmute",
                "moderator": self.client.user.id,
                "message_id": ""
            })
        else:
            if user in guild.members:
                if any([x.name.lower() == "muted" for x in guild.roles]):
                    role = [x for x in guild.roles if x.name.lower() =="muted"][0]
                    await user.remove_roles(role, reason="Time's up")

            if database.get_server(guild.id).get(Query().modlog) == None:
                mod_log_db = modlogs.get_server(guild.id)
                case_number = len(mod_log_db.all()) + 1
                mod_log_db.insert({
                    "case": case_number,
                    "reason": "Time's up",
                    "userid": user.id,
                    "operation": "unmute",
                    "moderator": self.client.user.id,
                    "message_id": ""
                })
            elif database.get_server(guild.id).get(Query().modlog)['modlog'] == "":
                mod_log_db = ctx['modlogs'].get_server(guild.id)
                case_number = len(mod_log_db.all()) + 1
                mod_log_db.insert({
                    "case": case_number,
                    "reason": "Time's up",
                    "userid": user.id,
                    "operation": "unmute",
                    "moderator": self.client.user.id,
                    "message_id": ""
                })
            else:

                try:
                    mod_log_db = modlogs.get_server(guild.id)
                    case_number = len(mod_log_db.all()) + 1
                    channel_to_send_to = guild.get_channel(int(database.get_server(guild.id).get(Query().modlog)['modlog']))
                    embed = Embed(colour=discord.Color.green())
                    embed.title = f"User unmuted"
                    embed.add_field(name="User:", value=f"{user.name}#{user.discriminator}")
                    embed.add_field(name="Reason:", value=f"Time's up")
                    embed.add_field(name="Moderator:", value=f"{self.client.user.name}#{self.client.user.discriminator}")
                    embed.add_field(name="Case Number", value=f"{case_number}")

                    message = await channel_to_send_to.send(embed=embed)

                    mod_log_db.insert({
                        "case": case_number,
                        "reason": "Time's up",
                        "userid": user.id,
                        "operation": "unmute",
                        "moderator": self.client.user.id,
                        "message_id": message.id
                    })


                except Exception as e:
                    mod_log_db.insert({
                        "case": case_number,
                        "reason": "Time's up",
                        "userid": user.id,
                        "operation": "unmute",
                        "moderator": self.client.user.id,
                        "message_id": 0
                    })

    def _unmute_queue(self, guild_id, user_id, time_to_unmute):
        current_time = round(get_time())
        sleep_time = (time_to_unmute - current_time)
        if sleep_time < 0:
            sleep_time = 0
        time.sleep(sleep_time)
        asyncio.run_coroutine_threadsafe(self.unmute_user(user_id, guild_id), self.client.loop)

    def start(self, guild_id, user_id, time_to_unmute):
        print(f"[Log] Starting Unmute Thread for [{guild_id}] for user [{user_id}]")
        status_thread = threading.Thread(target=self._unmute_queue, args=(guild_id, user_id, time_to_unmute))
        status_thread.start()
