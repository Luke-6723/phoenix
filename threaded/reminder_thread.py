from src.database_handler import Database
from tinydb import Query
import discord
import settings
import threading
import asyncio
import time
from time import time as get_time
users = Database("reminders")

class Reminder_Checks():

    def __init__(self, client):
        self.client = client

    def remove_reminder(self, userid, reminder):
        userdb = users.get_server(userid)
        current_db = userdb.get(Query().reminders)
        if current_db != None:
            user_reminders = current_db['reminders']
            user_reminders.remove(reminder)
            userdb.update({'reminders': user_reminders}, Query().reminders)



    async def send_reminders(self, reminder):
        user = reminder['user']
        reason = reminder['reason']
        await user.send(f"Hi there, You asked me to remind you:\n```{reason}```")

    def _remindmestatus(self, user, reason, time_until_reminder, reminder_dict):
        time.sleep(time_until_reminder)
        self.remove_reminder(user.id, reminder_dict)
        return asyncio.run_coroutine_threadsafe(self.send_reminders({
            'user': user,
            'reason': reason
        }),
            self.client.loop
        )


    def start(self, user, reason, time_until_reminder, reminder_dict):
        print(f"[Log] Starting Reminder Thread for {user.id}")
        status_thread = threading.Thread(target=self._remindmestatus, args=(user, reason, time_until_reminder, reminder_dict))
        status_thread.start()
