from src.database_handler import Database
from tinydb import Query
import discord
from discord import Embed
import settings
import threading
import asyncio
import time
from time import time as get_time
serversdb = Database("server_tempbans")
database = Database("database")
modlogs = Database("modlogs")


class Unban_Checks():

        def __init__(self, client):
            self.client = client

        async def unban_user(self, user_id, server_id):
            _server = serversdb.get_server(server_id)
            try:
                if _server.get(Query().tempbans) != None:
                    old_tempbans = _server.get(Query().tempbans)['tempbans']
                    new_tempbans = []
                    for ban in old_tempbans:
                        if ban.get(user_id, 0) != 0:
                            new_tempbans = old_tempbans.remove(ban)
                    _server.update({'tempbans': new_tempbans}, Query().tempbans)
            except Exception as e:
                print(e)
            user = await self.client.fetch_user(int(user_id))
            guild = self.client.get_guild(int(server_id))
            if user == None:
                mod_log_db = modlogs.get_server(guild.id)
                case_number = len(mod_log_db.all()) + 1
                mod_log_db.insert({
                    "case": case_number,
                    "reason": "Time's up",
                    "userid": user.id,
                    "operation": "tempban",
                    "moderator": self.client.user.id,
                    "message_id": ""
                })
            else:
                await guild.unban(user, reason="Time's up")

                if database.get_server(guild.id).get(Query().modlog) == None:
                    mod_log_db = modlogs.get_server(guild.id)
                    case_number = len(mod_log_db.all()) + 1
                    mod_log_db.insert({
                        "case": case_number,
                        "reason": "Time's up",
                        "userid": user.id,
                        "operation": "unban",
                        "moderator": self.client.user.id,
                        "message_id": ""
                    })
                elif database.get_server(guild.id).get(Query().modlog)['modlog'] == "":
                    mod_log_db = ctx['modlogs'].get_server(guild.id)
                    case_number = len(mod_log_db.all()) + 1
                    mod_log_db.insert({
                        "case": case_number,
                        "reason": "Time's up",
                        "userid": user.id,
                        "operation": "unban",
                        "moderator": self.client.user.id,
                        "message_id": ""
                    })
                else:

                    try:
                        mod_log_db = modlogs.get_server(guild.id)
                        case_number = len(mod_log_db.all()) + 1
                        channel_to_send_to = guild.get_channel(int(database.get_server(guild.id).get(Query().modlog)['modlog']))
                        embed = Embed(colour=discord.Color.green())
                        embed.title = f"User Unbanned"
                        embed.add_field(name="User:", value=f"{user.name}#{user.discriminator}")
                        embed.add_field(name="Reason:", value=f"Time's up")
                        embed.add_field(name="Moderator:", value=f"{self.client.user.name}#{self.client.user.discriminator}")
                        embed.add_field(name="Case Number", value=f"{case_number}")

                        message = await channel_to_send_to.send(embed=embed)

                        mod_log_db.insert({
                            "case": case_number,
                            "reason": "Time's up",
                            "userid": user.id,
                            "operation": "unban",
                            "moderator": self.client.user.id,
                            "message_id": message.id
                        })
                    except:
                        mod_log_db.insert({
                            "case": case_number,
                            "reason": "Time's up",
                            "userid": user.id,
                            "operation": "unban",
                            "moderator": self.client.user.id,
                            "message_id": 0
                        })





        def _unban_queue(self, guild, user_to_unban, time_to_unban):
            if (time_to_unban - round(get_time())) < 0:
                sleep_time = 0
            else:
                sleep_time = (time_to_unban - round(get_time()))

            time.sleep(sleep_time)
            asyncio.run_coroutine_threadsafe(self.unban_user(user_to_unban, guild), self.client.loop)



        def start(self, guild, user_to_unban, time_to_unban):
            print(f"[Log] Starting Unban from [{guild}] Thread for {user_to_unban.id}")
            status_thread = threading.Thread(target=self._unban_queue, args=(guild, user_to_unban.id, time_to_unban))
            status_thread.start()
