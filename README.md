Phoenix
======
A discord bot created by discord user: Luke#6723

Dependencies
======
[Discord.py](https://github.com/Rapptz/discord.py) [Rewrite]<br>
Install using this `py -m pip install -U discord.py`

Availability
======
[Github Repository](https://github.com/Luke-6723/Phoenix)
[GitLab Repository](https://gitlab.com/Luke-6723/Phoenix)
