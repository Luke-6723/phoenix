'''Error handler classes are stored here'''

class CommandNotFound(BaseException):
    '''Raised when command not found'''
