from src import database_handler
from threaded import unmute_thread
from tinydb import Query
from time import time as get_time

async def check_mutes_startup(CLIENT):
    server_mutes_db = database_handler.Database("server_mutes")
    for server in server_mutes_db._db.tables():
        server_mutes = server_mutes_db.get_server(server)
        if server_mutes.get(Query().mutes) != None:
            for mute in server_mutes.get(Query().mutes)['mutes']:
                userid = int(list(mute.keys())[0])
                time_to_unmute = int(list(mute.values())[0])
                if time_to_unmute == -1:
                    continue
                unmute_thread.Unmute_Checks(CLIENT).start(server, userid, time_to_unmute)
