'''Import required modules'''
from . import error_handler
from tinydb import Query

COMMANDS = {}

def register_command(commandname: str, command_class):
    '''Register command function'''
    COMMANDS[commandname] = command_class
    print("[Log] Command {commandname} has been registered".format(commandname=commandname))

async def run_command(command_to_run: str, ctx: dict, client, database):
    '''Run command'''
    server_commands = ctx['customcoms'].get_server(ctx['guild'].id)
    server_db = database.get_server(ctx['guild'].id)
    if command_to_run.lower() in COMMANDS:
        try:
            return await COMMANDS[command_to_run].run(ctx=ctx, client=client, database=database)
        except Exception as e:
            raise e
    elif server_db.get(Query().aliases) != None:
        aliases = server_db.get(Query().aliases)['aliases']
        if command_to_run in aliases:
            return await COMMANDS[aliases[command_to_run]].run(ctx=ctx, client=client, database=database)
    else:
        if server_commands.get(Query()[ctx['command_name']]) == None:
            return
        else:
            return await ctx['channel'].send(f"{server_commands.get(Query()[ctx['command_name']])[ctx['command_name']]}")
