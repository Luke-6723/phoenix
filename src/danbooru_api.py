import urllib3
import certifi
from urllib.parse import quote
import json
import random


def _params(**params):

	args = ""
	char = "?"

	for k, v in params.items():
		args += "{}{}={}".format(
			char, quote(str(k)), quote(str(v))
		)
		char = "&"

	return args

class Danbooru():

	def __init__(self):

		self._api = urllib3.PoolManager(
			cert_reqs="CERT_REQUIRED",
			ca_certs=certifi.where()
		)
		self._url_root = "https://danbooru.donmai.us"

	def search_tag(self, tag):

		p = _params(**{
			"limit": 1,
			"search[name_matches]": "*{}*".format(tag),
			"search[order]": "count"
		})

		url = self._url_root + "/tags.json" + p

		resp = self._api.request("GET", url)
		resp_json = json.loads(resp.data.decode("utf-8"))

		return dict(enumerate(resp_json)).get(0, {}).get("name", "")

	def random_with_tags(self, *tags, nsfw=False):

		realtags = []

		for t in tags:
			if len(t.split(":")) > 1:
				realtags.append(t)
			elif t:
				realtags.append(self.search_tag(t))

		p = _params(**{
			"limit": 10,
			"tags": " ".join(realtags),
			"random": "true"
		})

		url = self._url_root + "/posts.json" + p

		resp = self._api.request("GET", url)
		resp_json = json.loads(resp.data.decode("utf-8"))

		existing = [i["file_url"] for i in resp_json if i.get("file_url", False)]

		try:
			return random.choice(existing)
		except IndexError:
			return None
