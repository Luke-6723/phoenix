from src import database_handler
from threaded import unban_thread
from tinydb import Query
from time import time as get_time

async def check_unbans_startup(CLIENT):
    unbans = database_handler.Database("server_tempbans")
    for server in unbans._db.tables():
        server_tempbans = unbans.get_server(server)
        if server_tempbans.get(Query().tempbans) != None:
            for ban in server_tempbans.get(Query().tempbans)['tempbans']:
                userid = int(list(ban.keys())[0])
                time_to_unban = int(list(ban.values())[0])
                user_to_unban = await CLIENT.fetch_user(userid)
                unban_thread.Unban_Checks(CLIENT).start(server, user_to_unban, time_to_unban)
