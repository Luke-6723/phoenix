from src import database_handler
from threaded import reminder_thread
from tinydb import Query
from time import time as get_time

async def check_reminders_startup(CLIENT):
    users = database_handler.Database("reminders")
    for userid in users._db.tables():
        if users.get_server(userid).get(Query().reminders) is None:
            continue
        for reminder in users.get_server(userid).get(Query().reminders)['reminders']:
            user = await CLIENT.fetch_user(int(userid))
            time_passed = reminder['time_set'] - round(get_time())
            if (time_passed + reminder['time_until_reminder']) < 0:
                time_until_reminder = 1
            else:
                time_until_reminder = reminder['time_until_reminder'] - time_passed
            reminder_thread.Reminder_Checks(CLIENT).start(user, reminder['reason'], time_until_reminder, reminder)
