import requests
import json

class Youtube():
    def __init__(self):
        self.base_url = "https://youtube-scrape.herokuapp.com/api/search?q="

    def get_search(self, query: str):
        res = requests.get(url=(self.base_url + query))

        return res.json()
