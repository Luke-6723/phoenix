from src.database_handler import Database
from tinydb import Query

def tempbans_list_exists(server_id):
    server = Database('server_tempbans').get_server(server_id)
    tempbans = server.get(Query().tempbans)
    if tempbans == None:
        return False
    else:
        return True

def add_tempban(server_id, user_id, time_untempban):
    try:
        server = Database('server_tempbans').get_server(server_id)
        if not tempbans_list_exists(server_id):
            server.insert({"tempbans": []})
        tempbans = server.get(Query().tempbans)['tempbans']
        tempban_to_add = {
            str(user_id) : str(round(time_untempban))
        }
        if list(tempban_to_add.keys())[0] not in [list(thing.keys())[0] for thing in tempbans]:
            tempbans.append(tempban_to_add)
            server.update({"tempbans": tempbans}, Query().tempbans)
            return True
        else:
            return False
    except Exception as e:
        raise e

def get_all_tempbans(server_id):
    server = Database('server_tempbans').get_server(server_id)
    if not tempbans_list_exists(server_id):
        server.insert({"tempbans": []})
    tempbans = server.get(Query().tempbans)['tempbans']
    return tempbans
