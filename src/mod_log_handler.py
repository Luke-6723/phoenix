'''moderation logger handler'''
import discord
from discord import Embed
from tinydb import Query
from .check_permissions import check_permissions_channel

async def add_case(ctx, database, client, user, reason, operation, command_ran):
    if database.get_server(ctx['guild'].id).get(Query().modlog) == None:
        return
    elif database.get_server(ctx['guild'].id).get(Query().modlog)['modlog'] == "":
        return

    mod_log_db = ctx['modlogs'].get_server(ctx['guild'].id)
    case_number = len(mod_log_db.all()) + 1

    channel_to_send_to = ctx['guild'].get_channel(int(database.get_server(ctx['guild'].id).get(Query().modlog)['modlog']))

    if operation == "mute":
        operation_txt = "Muted"
        embed = Embed(colour=discord.Color.dark_orange())
    elif operation == "kick":
        operation_txt = "Kicked"
        embed = Embed(colour=discord.Color.dark_red())
    elif operation == "ban":
        operation_txt = "Banned"
        embed = Embed(colour=discord.Color.dark_red())
    elif operation == "unmute":
        operation_txt = "Unmuted"
        embed = Embed(colour=discord.Color.green())
    elif operation == "unban":
        operation_txt = "Unbanned"
        embed = Embed(colour=discord.Color.green())
    elif operation == "tempban":
        operation_txt = "Temporarily Banned"
        embed = Embed(colour=discord.Color.dark_red())

    embed.title = f"User {operation_txt}"
    embed.add_field(name="User:", value=f"{user.name}#{user.discriminator}")
    embed.add_field(name="Reason:", value=f"{reason}")
    embed.add_field(name="Moderator:", value=f"{ctx['author'].name}#{ctx['author'].discriminator}")
    embed.add_field(name="Case Number", value=f"{case_number}")

    if channel_to_send_to == None:
        message = await ctx['channel'].send(embed=embed)
        mod_log_db.insert({
            "case": case_number,
            "reason": reason,
            "userid": user.id,
            "operation": operation,
            "moderator": ctx['author'].id,
            "message_id": message.id
        })
        return await ctx['channel'].send("The channel currently set as mod-log seems to have been deleted\nSo sent message here instead.")
    elif check_permissions_channel(channel_to_send_to, ctx['guild'].get_member(client.user.id), {"send_messages": False}):
        message = await ctx['channel'].send(embed=embed)
        mod_log_db.insert({
            "case": case_number,
            "reason": reason,
            "userid": user.id,
            "operation": operation,
            "moderator": ctx['author'].id,
            "message_id": message.id
        })
        return await ctx['channel'].send("I do not have permission to send messages in the mod-log channel.\nSo sent message here instead.")

    message = await channel_to_send_to.send(embed=embed)
    mod_log_db.insert({
        "case": case_number,
        "reason": reason,
        "userid": user.id,
        "operation": operation,
        "moderator": ctx['author'].id,
        "message_id": message.id
    })

async def update_case(client, ctx, database, case_num, new_reason):
    original_case = ctx['modlogs'].get_server(ctx['guild'].id).get(Query().case == case_num)
    mod_log_channel = ctx['guild'].get_channel(int(database.get_server(ctx['guild'].id).get(Query().modlog)['modlog']))
    mod_log_db = ctx['modlogs'].get_server(ctx['guild'].id)

    if original_case == None:
        return await ctx['channel'].send("That case does not exist")

    operation = original_case['operation']

    user = await client.fetch_user(original_case['userid'])

    if operation == "mute":
        operation_txt = "Muted"
        embed = Embed(colour=discord.Color.dark_orange())
    elif operation == "kick":
        operation_txt = "Kicked"
        embed = Embed(colour=discord.Color.dark_red())
    elif operation == "ban":
        operation_txt = "Banned"
        embed = Embed(colour=discord.Color.dark_red())
    elif operation == "unmute":
        operation_txt = "Unmuted"
        embed = Embed(colour=discord.Color.green())
    elif operation == "unban":
        operation_txt = "Unbanned"
        embed = Embed(colour=discord.Color.green())

    embed.title = f"User {operation_txt}"
    embed.add_field(name="User:", value=f"{user.name}#{user.discriminator}")
    embed.add_field(name="Reason:", value=f"{new_reason}")
    embed.add_field(name="Moderator:", value=f"{ctx['author'].name}#{ctx['author'].discriminator}")
    embed.add_field(name="Case Number", value=f"{case_num}")

    updated_case = {
        'case': original_case['case'],
        'reason': new_reason,
        'userid': original_case['userid'],
        'operation': original_case['operation'],
        'moderator': ctx['author'].id,
        'message_id': original_case['message_id']
    }

    try:
        mod_log_db.update(updated_case, Query().case == case_num)
        case_msg = await mod_log_channel.fetch_message(original_case['message_id'])
        await case_msg.edit(embed=embed)
        return await ctx['channel'].send("Case updated.")
    except Exception as e:
        if e.status == 404:
            mod_log_db.update(updated_case, Query().case == case_num)
            return await ctx['channel'].send(f"That case message was deleted. Updated Database.\nDo `{ctx['prefix']} case {case_num}` to see the case info")
        else:
            return await ctx['channel'].send("There was an error processing that command. Please ask the author to check for you.")

async def update_case_multiple(client, ctx, database, cases, new_reason):
    failed_cases = []
    dupe_cases = []
    complete_cases = []
    for case_num in cases:
        try:
            if case_num in complete_cases or case_num in failed_cases:
                if case_num not in dupe_cases:
                    dupe_cases.append(case_num)
                continue
            original_case = ctx['modlogs'].get_server(ctx['guild'].id).get(Query().case == int(case_num))
            mod_log_channel = ctx['guild'].get_channel(int(database.get_server(ctx['guild'].id).get(Query().modlog)['modlog']))
            mod_log_db = ctx['modlogs'].get_server(ctx['guild'].id)

            if original_case == None:
                raise KeyError("Case does not exist")

            operation = original_case['operation']

            user = await client.fetch_user(original_case['userid'])

            if operation == "mute":
                operation_txt = "Muted"
                embed = Embed(colour=discord.Color.dark_orange())
            elif operation == "kick":
                operation_txt = "Kicked"
                embed = Embed(colour=discord.Color.dark_red())
            elif operation == "ban":
                operation_txt = "Banned"
                embed = Embed(colour=discord.Color.dark_red())
            elif operation == "unmute":
                operation_txt = "Unmuted"
                embed = Embed(colour=discord.Color.green())
            elif operation == "unban":
                operation_txt = "Unbanned"
                embed = Embed(colour=discord.Color.green())

            embed.title = f"User {operation_txt}"
            embed.add_field(name="User:", value=f"{user.name}#{user.discriminator}")
            embed.add_field(name="Reason:", value=f"{new_reason}")
            embed.add_field(name="Moderator:", value=f"{ctx['author'].name}#{ctx['author'].discriminator}")
            embed.add_field(name="Case Number", value=f"{case_num}")

            updated_case = {
                'case': original_case['case'],
                'reason': new_reason,
                'userid': original_case['userid'],
                'operation': original_case['operation'],
                'moderator': ctx['author'].id,
                'message_id': original_case['message_id']
            }
            try:
                mod_log_db.update(updated_case, Query().case == int(case_num))
                case_msg = await mod_log_channel.fetch_message(original_case['message_id'])
                await case_msg.edit(embed=embed)
                complete_cases.append(case_num)
            except:
                failed_cases.append(case_num)

        except:
            failed_cases.append(case_num)
            continue
    return {
        'failed_cases': failed_cases,
        'dupe_cases': dupe_cases,
        'complete_cases': complete_cases
    }
