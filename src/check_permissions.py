
def check_permissions(msg, perms):
    ch = msg.channel
    author = msg.author
    resolved = ch.permissions_for(author)
    return all(getattr(resolved, name, None) == value for name, value in perms.items())

def check_permissions_channel(ch, user, perms):
    resolved = ch.permissions_for(user)
    return all(getattr(resolved, name, None) == value for name, value in perms.items())
