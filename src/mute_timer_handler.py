from src.database_handler import Database
from tinydb import Query

def mutes_list_exists(server_id):
    server = Database('server_mutes').get_server(server_id)
    mutes = server.get(Query().mutes)
    if mutes == None:
        return False
    else:
        return True

def add_mute(server_id, user_id, time_unmute):
    try:
        server = Database('server_mutes').get_server(server_id)
        if not mutes_list_exists(server_id):
            server.insert({"mutes": []})
        mutes = server.get(Query().mutes)['mutes']
        mute_to_add = {
            str(user_id) : str(time_unmute)
        }
        if mute_to_add not in mutes:
            mutes.append(mute_to_add)
            server.update({"mutes": mutes}, Query().mutes)
    except Exception as e:
        raise e

def get_all_mutes(server_id):
    server = Database('server_mutes').get_server(server_id)
    if not mutes_list_exists(server_id):
        server.insert({"mutes": []})
    mutes = server.get(Query().mutes)['mutes']
    return mutes
