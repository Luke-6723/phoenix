'''Import required modules'''
from tinydb import TinyDB

class Database():
    '''Database Handler'''
    def __init__(self, databasename: str):
        TinyDB.DEFAULT_TABLE = "556013291378442240"
        self._db = TinyDB('./database/{database}.json'.format(database=databasename), sort_keys=True, indent=2)

    def _get_all_keys(self):
        return self._db.all()

    def remove_server(self, _id):
        '''Remove server from Database'''
        return self._db.purge_table(str(_id))

    def get_server(self, _id):
        '''Get server from Database'''
        return self._db.table(str(_id))
