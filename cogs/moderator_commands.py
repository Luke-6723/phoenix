'''Moderator Commands'''
from src.check_permissions import check_permissions, check_permissions_channel
from src import mod_log_handler
from tinydb import Query
from src.database_handler import Database
import src.mute_timer_handler as sm
import src.tempban_timer_handler as stb
from time import time as get_time
import discord
from settings import eval_users
from src import command_registery
from threaded import unban_thread, unmute_thread

class Kick():
    '''Kick command'''
    def __init__(self):
        self.name = "kick"
        self.example = "{prefix} kick [user id] -r [reason]"
        self.desc = "Kick a user from the server"
        self.permission_requirement = "moderator"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        '''Run kick command'''
        if check_permissions(ctx['_main'], {'kick_members': True}) is False:
            return await ctx['channel'].send("You do not have permission to do that.")
        try:
            if len(ctx['command_args']) > 2:
                if ctx['command_args'][1] == "-r":
                    reason = " ".join(ctx['command_args'][2:])
                else:
                    reason = f"Kicked by {ctx['author']}"
            else:
                reason = f"Kicked by {ctx['author']}"

            try:
                user = ctx['guild'].get_member(int(ctx['command_args'][0].replace("<@", "").replace(">", "").replace("!", "")))
            except:
                return await ctx['channel'].send("Please mention the user or give an id.")
            if user == None:
                return await ctx['channel'].send("That user is not currently in the server.")
            await ctx['guild'].kick(user, reason=reason)
            await mod_log_handler.add_case(ctx, database, client, user, reason, "kick", command_ran=True)
            return await ctx['channel'].send(f"Kicked user `{user}`. Reason: `{reason}`")
        except Exception as e:
                try:
                    if(e.status == 403):
                        await ctx['channel'].send("I do not have permission to kick this user.")
                except:
                    print(e)

class TempBan():
    '''Tempban command'''
    def __init__(self):
        self.name = "tempban"
        self.example = "{prefix} tempban [user id] -r [reason] -t [time]"
        self.desc = "tempban a user in the server"
        self.permission_requirement = "moderator"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        if ctx['author'].id in eval_users:
            pass
        elif check_permissions(ctx['_main'], {'ban_members': True}) is False:
            return await ctx['channel'].send("You do not have permission to do that.")

        if check_permissions_channel(ctx['channel'], ctx['guild'].me, {'ban_members': True}) is False:
            return await ctx['channel'].send("I do not have permission to ban members")

        if len(ctx['command_args']) == 0:
            return await ctx['channel'].send("Please give user to tempban.")

        if len(ctx['command_args']) == 1:
            return await ctx['channel'].send(f"If you're not going to give a reason or a time, please use `{ctx['prefix']} ban` instead")

        if len(ctx['command_args']) > 2:
            if "-r" not in ctx['command_args']:
                return await ctx['channel'].send(f"If you're not going to give a reason, please use `{ctx['prefix']} ban` instead")
            if "-t" not in ctx['command_args']:
                return await ctx['channel'].send(f"If you're not going to give a time, please use `{ctx['prefix']} ban` instead")

            if "-r" and "-t" in ctx['command_args']:
                user_id = ctx['command_args'][0].replace("<@", "").replace(">", "").replace("!", "")
                reason_and_time = " ".join(ctx['command_args'][1:])
                time_unformatted = reason_and_time.split("-t")[1]
                reason = reason_and_time.split("-t")[0][2:]
                current_time = get_time()

                time_str = "".join([i for i in time_unformatted if i.isdigit()])
                time_length = "".join([i for i in time_unformatted if not i.isdigit()]).split()[0]
                if time_str == "":
                    time = 1
                else:
                    time = int(time_str)
                if time_length == "":
                    time_length == "s"


                if time_length in ["s", "sec", "secs", "seconds"]:
                    if time <= 1:
                        time_txt = "Second"
                        reason += f" ({time} {time_txt})"
                        time_to_unban = time * 1
                    else:
                        time_txt = "Seconds"
                        time_to_unban = time * 1
                        reason += f" ({time} {time_txt})"
                    user_id = int(ctx['command_args'][0].replace("<@", "").replace(">", "").replace("!", ""))
                    user_tempbanned = stb.add_tempban(str(ctx['guild'].id), user_id, (current_time + time_to_unban))
                elif time_length in ["m", "minute", "minutes", "mins"]:
                    if time <= 1:
                        time_txt = "Minute"
                        reason += f" ({time} {time_txt})"
                        time_to_unban = time * 60
                    else:
                        time_txt = "Minutes"
                        time_to_unban = time * 60
                        reason += f" ({time} {time_txt})"
                    user_id = int(ctx['command_args'][0].replace("<@", "").replace(">", "").replace("!", ""))
                    user_tempbanned = stb.add_tempban(str(ctx['guild'].id), user_id, (current_time + time_to_unban))
                elif time_length in ["month", "months"]:
                    if time <= 1:
                        time_txt = "month"
                        reason += f" ({time} {time_txt})"
                        time_to_unban = time * 2592000
                    else:
                        time_txt = "months"
                        time_to_unban = time * 2592000
                        reason += f" ({time} {time_txt})"
                    user_id = int(ctx['command_args'][0].replace("<@", "").replace(">", "").replace("!", ""))
                    user_tempbanned = stb.add_tempban(ctx['guild'].id, user_id, (current_time + time_to_unban))
                elif time_length in ['hour', 'hr', 'hours', 'h']:
                    if time <= 1:
                        time_txt = "Hour"
                        reason += f" ({time} {time_txt})"
                        time_to_unban = time * 3600
                    else:
                        time_txt = "Hours"
                        time_to_unban = time * 3600
                        reason += f" ({time} {time_txt})"
                    user_id = int(ctx['command_args'][0].replace("<@", "").replace(">", "").replace("!", ""))
                    user_tempbanned = stb.add_tempban(ctx['guild'].id, user_id, time_to_unban)
                elif time_length in ['day', 'days', 'd']:
                    if time <= 1:
                        time_txt = "Day"
                        reason += f" ({time} {time_txt})"
                        time_to_unban = time * 86400
                    else:
                        time_txt = "Days"
                        time_to_unban = time * 86400
                        reason += f" ({time} {time_txt})"
                    user_id = int(ctx['command_args'][0].replace("<@", "").replace(">", "").replace("!", ""))
                    user_tempbanned = stb.add_tempban(ctx['guild'].id, user_id, (current_time + time_to_unban))
                elif time_length in ['year', 'years', 'yr', 'yrs', 'y']:
                    if time <= 1:
                        time_txt = "Year"
                        reason += f" ({time} {time_txt})"
                        time_to_unban = time * 31536000
                    else:
                        time_txt = "Years"
                        time_to_unban = time * 31536000
                        reason += f" ({time} {time_txt})"
                    user_id = int(ctx['command_args'][0].replace("<@", "").replace(">", "").replace("!", ""))
                    user_tempbanned = stb.add_tempban(ctx['guild'].id, user_id, (current_time + time_to_unban))

                if not user_tempbanned:
                    return await ctx['channel'].send("User is already tempbanned")

                user = await client.fetch_user(user_id)

                unban_thread.Unban_Checks(client).start(ctx['guild'].id, user, (current_time + time_to_unban))
                await mod_log_handler.add_case(ctx, database, client, user, reason, "tempban", command_ran=True)
                await ctx['guild'].ban(user, reason=reason)
                await ctx['channel'].send(f"User `{user}` Temporarily banned for `{reason}`")

class Unban():
    '''Unban command'''
    def __init__(self):
        self.name = "unban"
        self.example = "{prefix} unban [user id] -r [reason]"
        self.desc = "Unban a user from the server"
        self.permission_requirement = "moderator"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        '''Run unban command'''
        if ctx['author'].id in eval_users:
            pass
        elif check_permissions(ctx['_main'], {'ban_members': True}) is False:
            return await ctx['channel'].send("You do not have permission to do that.")
        try:
            if len(ctx['command_args']) > 2 :
                if ctx['command_args'][1] == "-r":
                    reason = " ".join(ctx['command_args'][2:])
                else:
                    reason = f"Unbanned by {ctx['author']}"
            else:
                reason = f"Unbanned by {ctx['author']}"

            try:
                user = await client.fetch_user(int(ctx['command_args'][0].replace("<@", "").replace(">", "").replace("!", "")))
            except:
                return await ctx['channel'].send("Please mention the user or give an id.")

            bans = await ctx['guild'].bans()
            banned_users = [ban.user for ban in bans]
            if user not in banned_users:
                return await ctx['channel'].send(f"`{user}` is not banned")

            tempbans = Database("server_tempbans").get_server(ctx['guild'].id)
            if tempbans.get(Query().tempbans) != None:
                old_tempbans = tempbans.get(Query().tempbans)['tempbans']
                for ban in old_tempbans:
                    if ban.get(str(user.id), 0) != 0:
                        new_tempbans = old_tempbans.remove(ban)
                tempbans.update({'tempbans': old_tempbans}, Query().tempbans)

            await ctx['guild'].unban(user, reason=reason)
            await mod_log_handler.add_case(ctx, database, client, user, reason, "unban", command_ran=True)
            return await ctx['channel'].send(f"Unbanned user `{user}`. Reason: `{reason}`")
        except Exception as e:
                try:
                    if(e.status == 403):
                        return await ctx['channel'].send("I do not have permission to ban this user.")
                except:
                    print(e)

class Ban():
    '''Ban command'''
    def __init__(self):
        self.name = "ban"
        self.example = "{prefix} ban [user id] -r [reason]"
        self.desc = "Ban a user from the server"
        self.permission_requirement = "moderator"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        '''Run ban command'''
        if check_permissions(ctx['_main'], {'ban_members': True}) is False:
            return await ctx['channel'].send("You do not have permission to do that.")

        try:
            if len(ctx['command_args']) > 2 :
                if ctx['command_args'][1] == "-r":
                    reason = " ".join(ctx['command_args'][2:])
                else:
                    reason = f"Banned by {ctx['author']}"
            else:
                reason = f"Banned by {ctx['author']}"

            try:
                user = await client.fetch_user(int(ctx['command_args'][0].replace("<@", "").replace(">", "").replace("!", "")))
            except:
                return await ctx['channel'].send("Please mention the user or give an id.")
            await ctx['guild'].ban(user, reason=reason)
            await mod_log_handler.add_case(ctx, database, client, user, reason, "ban", command_ran=True)
            return await ctx['channel'].send(f"Banned user `{user}`. Reason: `{reason}`")
        except Exception as e:
                try:
                    if(e.status == 403):
                        await ctx['channel'].send("I do not have permission to ban this user.")
                except:
                    print(e)

class Mute():
    '''Mute command'''
    def __init__(self):
        self.name = "mute"
        self.example = "{prefix} mute [user id] -r [reason] -t [time]"
        self.desc = "Mute a user in the server"
        self.permission_requirement = "moderator"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        '''Run Mute command'''
        async def mute_user(user, time_to_unmute, reason):
            roles = ctx['guild'].roles
            time_to_unmute += round(get_time())
            if any([x.name.lower() == "muted" for x in roles]):
                role = [x for x in ctx['guild'].roles if x.name.lower() =="muted"][0]
                try:
                    if role in user.roles:
                        return await ctx['channel'].send("That user is already muted")

                    if time_to_unmute > 0:
                        guild_id = ctx['guild'].id
                        unmute_thread.Unmute_Checks(client).start(guild_id, user_id, time_to_unmute)

                    sm.add_mute(ctx['guild'].id, user_id, time_to_unmute)

                    await user.add_roles(role, reason=reason)
                    await mod_log_handler.add_case(ctx, database, client, user, reason, "mute", command_ran=True)
                    return await ctx['channel'].send(f"User `{user}` muted for: `{' '.join(reason.split())}`")
                except discord.errors.Forbidden:
                    await ctx['channel'].send("I do not have permission to mute this user.")

            else:
                return await ctx['channel'].send("There is no role called `Muted`. Please create one.")

        if check_permissions(ctx['_main'], {'manage_roles': True}) is False:
            return await ctx['channel'].send("You do not have permission to do that.")

        if "-r" in ctx['command_args']:
            user_id = ctx['command_args'][0].replace("<@", "").replace(">", "").replace("!", "")
            reason_and_time = " ".join(ctx['command_args'][1:])
            time_unformatted = reason_and_time.split("-t")[1]
            reason = reason_and_time.split("-t")[0][2:]
            current_time = get_time()

            try:
                user = ctx['guild'].get_member(int(ctx['command_args'][0].replace("<@", "").replace(">", "").replace("!", "")))
            except:
                return await ctx['channel'].send("Please mention the user or give an id.")
            if user == None:
                return await ctx['channel'].send("That user is not currently in the server.")

            if "-t" in ctx['command_args']:
                time_str = "".join([i for i in time_unformatted if i.isdigit()])
                time_length = "".join([i for i in time_unformatted if not i.isdigit()]).split()[0]
                if time_str == "":
                    time = 1
                else:
                    time = int(time_str)
                if time_length == "":
                    time_length == "s"


                if time_length in ["s", "sec", "secs", "seconds"]:
                    if time <= 1:
                        time_txt = "Second"
                        reason += f" ({time} {time_txt})"
                        time_to_unmute = time
                    else:
                        time_txt = "Seconds"
                        time_to_unmute = time
                        reason += f" ({time} {time_txt})"
                    user_id = int(ctx['command_args'][0].replace("<@", "").replace(">", "").replace("!", ""))
                    await mute_user(user, time_to_unmute, reason)
                elif time_length in ["m", "minute", "minutes", "mins"]:
                    if time <= 1:
                        time_txt = "Minute"
                        reason += f" ({time} {time_txt})"
                        time_to_unmute = time * 60
                    else:
                        time_txt = "Minutes"
                        time_to_unmute = time * 60
                        reason += f" ({time} {time_txt})"
                    user_id = int(ctx['command_args'][0].replace("<@", "").replace(">", "").replace("!", ""))
                    await mute_user(user, time_to_unmute, reason)
                elif time_length in ["month", "months"]:
                    if time <= 1:
                        time_txt = "month"
                        reason += f" ({time} {time_txt})"
                        time_to_unmute = time * 2592000
                    else:
                        time_txt = "months"
                        time_to_unmute = time * 2592000
                        reason += f" ({time} {time_txt})"
                    user_id = int(ctx['command_args'][0].replace("<@", "").replace(">", "").replace("!", ""))
                    await mute_user(user, time_to_unmute, reason)
                elif time_length in ['hour', 'hr', 'hours', 'h']:
                    if time <= 1:
                        time_txt = "Hour"
                        reason += f" ({time} {time_txt})"
                        time_to_unmute = time * 3600
                    else:
                        time_txt = "Hours"
                        time_to_unmute = time * 3600
                        reason += f" ({time} {time_txt})"
                    user_id = int(ctx['command_args'][0].replace("<@", "").replace(">", "").replace("!", ""))
                    await mute_user(user, time_to_unmute, reason)
                elif time_length in ['day', 'days', 'd']:
                    if time <= 1:
                        time_txt = "Day"
                        reason += f" ({time} {time_txt})"
                        time_to_unmute = time * 86400
                    else:
                        time_txt = "Days"
                        time_to_unmute = time * 86400
                        reason += f" ({time} {time_txt})"
                    user_id = int(ctx['command_args'][0].replace("<@", "").replace(">", "").replace("!", ""))
                    await mute_user(user, time_to_unmute, reason)
                elif time_length in ['year', 'years', 'yr', 'yrs', 'y']:
                    if time <= 1:
                        time_txt = "Year"
                        reason += f" ({time} {time_txt})"
                        time_to_unmute = time * 31536000
                    else:
                        time_txt = "Years"
                        time_to_unmute = time * 31536000
                        reason += f" ({time} {time_txt})"
                    user_id = int(ctx['command_args'][0].replace("<@", "").replace(">", "").replace("!", ""))
                    await mute_user(user, time_to_unmute, reason)
            else:
                time_to_unmute = -1
                await mute_user(user, time_to_unmute, reason)

class Unmute():
    '''Unmute command'''
    def __init__(self):
        self.name = "unmute"
        self.example = "{prefix} unmute [user id] -r [reason]"
        self.desc = "Unmute a user in the server"
        self.permission_requirement = "moderator"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        '''Run Unmute command'''
        if check_permissions(ctx['_main'], {'manage_roles': True}) is False:
            return await ctx['channel'].send("You do not have permission to do that.")

        try:
            if len(ctx['command_args']) > 2 :
                if ctx['command_args'][1] == "-r":
                    reason = " ".join(ctx['command_args'][2:])
                else:
                    reason = f"Unmuted by {ctx['author']}"
            else:
                reason = f"Unmuted by {ctx['author']}"
        except:
            reason = f"Unmuted by {ctx['author']}"

        try:
            user = ctx['guild'].get_member(int(ctx['command_args'][0].replace("<@", "").replace(">", "").replace("!", "")))
        except:
            return await ctx['channel'].send("Please mention the user or give an id.")
        if user == None:
            return await ctx['channel'].send("That user is not currently in the server.")

        roles = ctx['guild'].roles
        if any([x.name.lower() == "muted" for x in roles]):
            role = [x for x in ctx['guild'].roles if x.name.lower() =="muted"][0]
            try:
                if role not in user.roles:
                    return await ctx['channel'].send("That user is not muted")
                await user.remove_roles(role, reason=reason)
                await mod_log_handler.add_case(ctx, database, client, user, reason, "unmute", command_ran=True)

                mutes = Database('server_mutes').get_server(ctx['guild'].id)

                if mutes.get(Query().mutes) == None:
                    mutes.insert({'mutes': []})
                mute_list = mutes.get(Query().mutes)['mutes']
                for mute in mute_list:
                    if str(user.id) in mute.keys():
                        mute_list.remove(mute)

                mutes.update({'mutes': mute_list}, Query().mutes)

                return await ctx['channel'].send(f"User `{user}` Unmuted, Reason: `{reason}`")
            except Exception as e:
                try:
                    if(e.status == 403):
                        await ctx['channel'].send("I do not have permission to mute this user.")
                except:
                    raise e

        else:
            return await ctx['channel'].send("There is no role called `Muted`. Please create one.")

class Reason():
    '''Reason command'''
    def __init__(self):
        self.name = "reason"
        self.example = "{prefix} reason [case number]  [new reason]\n{prefix} reason [case numbers] | [new reason]"
        self.desc = "Modifify the reason of a case"
        self.permission_requirement = "moderator"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        if check_permissions(ctx['_main'], {'manage_roles': False}):
            return await ctx['channel'].send("You do not have permission to do that.")

        if "|" not in ctx['command_args']:
            if len(ctx['command_args']) == 1:
                return await ctx['channel'].send("Please provide a reason")
            elif len(ctx['command_args']) == 0:
                return await ctx['channel'].send("Please provide a case number")

            try:
                case_num = int(ctx['command_args'][0])
            except:
                return await ctx['channel'].send("Please provide a valid number.")

            new_reason = " ".join(ctx['command_args'][1:])

            return await mod_log_handler.update_case(client, ctx, database, case_num, new_reason)
        else:
            cases = " ".join(ctx['command_args']).split("|")[0].split()
            new_reason = " ".join(ctx['command_args']).split("|")[1][1:]
            if len(cases) == 0:
                return await ctx['channel'].send("Please provide some case numbers")
            if len(cases) > 5:
                return await ctx['channel'].send("Please only provide 5 cases at a time\n**This is mainly for ratelimit reasons**")
            if len(new_reason) == 0:
                return await ctx['channel'].send("Please provide a reason")

            case_result = await mod_log_handler.update_case_multiple(client, ctx, database, cases, new_reason)
            dupes = case_result['dupe_cases']
            fails = case_result['failed_cases']
            complete = case_result['complete_cases']
            embed=discord.Embed(color=discord.Color.dark_orange())
            embed.add_field(name="New Reason", value=f"``` {new_reason}```", inline=False)
            if len(complete) > 0:
                embed.add_field(name="Completed Case Updated", value=f"``` {', '.join(complete)}```", inline=False)
            if len(fails) > 0:
                embed.add_field(name="Failed Case Updates", value=f"``` {', '.join(fails)}```", inline=False)
            if len(dupes) > 0:
                embed.add_field(name="Case Dupes [Numbers you input twice]", value=f"``` {', '.join(dupes)}```", inline=False)
            return await ctx['channel'].send(embed=embed)



class Case():
    '''Case command'''
    def __init__(self):
        self.name = "case"
        self.example = "{prefix} case [case number]"
        self.desc = "Get a cases information"
        self.permission_requirement = "moderator"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        if check_permissions(ctx['_main'], {'manage_roles': False}):
            return await ctx['channel'].send("You do not have permission to do that.")

        if len(ctx['command_args']) >= 1:
            guild_modlogs = ctx['modlogs'].get_server(ctx['guild'].id)
            guild_db = database.get_server(ctx['guild'].id)

            try:
                case_num = int(ctx['command_args'][0])
            except:
                return await ctx['channel'].send("Please provide a valid number")

            mod_log_fetch = guild_modlogs.get(Query().case == case_num)

            if mod_log_fetch == None:
                return await ctx['channel'].send("That case does not exist")

            try:
                moderator = await client.fetch_user(mod_log_fetch['moderator'])
            except:
                moderator = "Unknown"
            user = await client.fetch_user(mod_log_fetch['userid'])

            if mod_log_fetch['operation'] == "mute":
                operation_txt = "Muted"
            elif mod_log_fetch['operation'] == "kick":
                operation_txt = "Kicked"
            elif mod_log_fetch['operation'] == "ban":
                operation_txt = "Banned"
            elif mod_log_fetch['operation'] == "unmute":
                operation_txt = "Unmuted"

            case_message = f"**Case number**: {mod_log_fetch['case']}\n**Moderator**: {moderator}\n**Operation**: {operation_txt}\n**User**: {user}\n**Reason**: {mod_log_fetch['reason']}"
            return await ctx['channel'].send(case_message)

        else:
            await ctx['channel'].send("Provide a case number")

def init_commands():
    Case()
    Reason()
    Kick()
    Ban()
    Mute()
    Unmute()
    Unban()
    TempBan()
