'''Basic command classes'''
from src.command_registery import COMMANDS as commands
from src.database_handler import Database
from random import randint
from discord import Embed
import discord
import time
from time import time as get_time
import re
from tinydb import Query
import psutil
import os
from src.danbooru_api import Danbooru
from src.youtube_handler import Youtube
from src import mute_timer_handler, command_registery
from src.check_permissions import check_permissions_channel
from src import database_handler
import random
from threaded import reminder_thread

process = psutil.Process(os.getpid())
cpupercent = process.cpu_percent()
memusage = process.memory_info().rss
yt = Youtube()

class Ping():
    '''ping commands'''
    def __init__(self):
        self.name = "ping"
        self.example = "{prefix} ping"
        self.desc = "Gives the response time of the bot"
        self.permission_requirement = "basic"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        '''run command'''
        return await ctx['channel'].send("Pong!")

class Support():
    '''ping commands'''
    def __init__(self):
        self.name = "support"
        self.example = "{prefix} support"
        self.desc = "Gives you a link to the support server"
        self.permission_requirement = "basic"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        '''run command'''
        embed = Embed(color=discord.Color.dark_orange())
        embed.title = "Phoenix Support"
        embed.description = "[Click this link to join Phoenix Support](https://discord.gg/34k4Mk2)"
        return await ctx['channel'].send(embed=embed)

class ListAliases():
    def __init__(self):
        self.name = "listaliases"
        self.example = "{prefix} listaliases"
        self.desc = "List all aliases on the server"
        self.permission_requirement = "basic"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        server_db = database.get_server(ctx['guild'].id)
        if server_db.get(Query().aliases) == None or len(server_db.get(Query().aliases)['aliases']) == 0:
            return await ctx['channel'].send("There are no aliases on this server.")

        async def get_seperator_length(list, seperator: int):
            longest = 0
            for thing in list:
                if len(thing) > longest:
                    longest = len(thing)
            return longest + seperator

        aliases = server_db.get(Query().aliases)['aliases']
        aliases_txt = "css\n[Alias] <Command>\n"
        seperator = await get_seperator_length(list(aliases.keys()), 3)
        for alias_name, command_name in aliases.items():
            aliases_txt += f"[{alias_name}]{' '*(seperator-len(alias_name))}=> <{command_name}>\n"

        return await ctx['channel'].send(f"Current Aliases on this server:\n```{aliases_txt}```")






class Suggest():
    def __init__(self):
        self.name = "suggest"
        self.example = "{prefix} suggest [feature]"
        self.desc = "Suggest a feature for the bot!"
        self.permission_requirement = "basic"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        '''run command'''
        blacklist_db = database_handler.Database("blacklist").get_server(0)
        if blacklist_db.get(Query().users) == None:
            blacklist_db.insert({"users": []})
        if len(ctx['command_args']) < 1:
            return await ctx['channel'].send("Please give an actually suggestion.")

        blacklisted = blacklist_db.get(Query().users)['users']
        if ctx['author'].id in blacklisted:
            embed = Embed(color=discord.Color.dark_red())
            embed.title = "You have been blacklisted from using this command."
            embed.description = "If you would like to appeal please join the support server.\n[Support server invite](https://discord.gg/34k4Mk2)"
            embed.set_footer(icon_url=client.user.avatar_url, text="Phoenix Blacklist")
            return await ctx['channel'].send(embed=embed)

        embed = Embed(color=discord.Color.dark_orange())
        suggestion_channel_id = 556880171672535043
        suggestion_channel = client.get_channel(suggestion_channel_id)
        suggestion_content = ctx['_main'].content[(len(self.name) + len(ctx['prefix']) + 1):]
        embed.title = "Phoenix Suggestion"
        embed.description = suggestion_content
        embed.set_footer(text=f"Suggestion given by: {ctx['author']} ({ctx['author'].id})", icon_url=ctx['author'].avatar_url)
        message = await suggestion_channel.send(embed=embed)
        emoji_no = client.get_emoji(571791161971310612)
        emoji_yes = client.get_emoji(571791161887293441)
        await message.add_reaction(emoji=emoji_yes)
        await message.add_reaction(emoji=emoji_no)
        embed = Embed(color=discord.Colour.dark_orange())
        embed.title = "Phoenix Suggestion Sent"
        return await ctx['channel'].send(embed=embed)

class Staff():
    def __init__(self):
        self.name = "staff"
        self.example = "{prefix} staff\nDo `{prefix} staff roles` to see the roles required for each catagory"
        self.desc = "Shows the staff in the server you are currently in."
        self.permission_requirement = "basic"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        '''Run command'''
        server_data = database.get_server(ctx['guild'].id)
        staff_roles = server_data.get(Query().staffroles)['staffroles']
        if server_data.get(Query().staffroles) == None:
            server_data.insert({"staffroles": {
                    "admin": [],
                    "mod": [],
                    "helper": [],
                }
            })
        embed = Embed(color=discord.Color.dark_gold())
        status_icons = {
            'online': '<:online:567036794999406622>',
            'offline': '<:offline:567036795171373066>',
            'dnd': '<:dnd:567036795259453455>',
            'away': '<:away:567036794659405830>'
        }
        admins = []
        mods = []
        helpers = []

        if len(ctx['command_args']) > 0 and ctx['command_args'][0] == "roles":
            embed.add_field(name="Admin:", value="Give a user a role named `Admin`, `Admins` `Administrator`")
            embed.add_field(name="Moderator", value="Give a user a role named `Mod`, `Mods`, `Moderator` or `Moderators`")
            embed.add_field(name="Helper", value="Give a user a role named `Helper` or `Helpers`")
            return await ctx['channel'].send(embed=embed)

        roles = ctx['guild'].roles
        roles_named = {}

        for role in ctx['guild'].roles:
            roles_named[role.name.lower()] = role

        if roles_named.get("admin", None):
            for user in roles_named.get("admin", None).members:
                    admins.append(user)

        if roles_named.get("admins", None):
            for user in roles_named['admins'].members:
                if user not in admins :
                    admins.append(user)

        if roles_named.get("administrator", None):
            for user in roles_named['administrator'].members:
                if user not in admins:
                    admins.append(user)


        for id in staff_roles['admin']:
            role_x = ctx['guild'].get_role(id)
            for user in role_x.members:
                if user not in admins:
                    admins.append(user)

        if roles_named.get("mod", None):
            for user in roles_named.get("mod", None).members:
                if user not in admins:
                    mods.append(user)

        if roles_named.get("moderator", None):
            for user in roles_named['moderator'].members:
                if user not in mods and user not in admins:
                    mods.append(user)

        if roles_named.get("moderators", None):
            for user in roles_named['moderators'].members:
                if user not in mods and user not in admins:
                    mods.append(user)

        for id in staff_roles['mod']:
            role_x = ctx['guild'].get_role(id)
            for user in role_x.members:
                if user not in admins and user not in mods:
                    mods.append(user)

        if roles_named.get("helper", None):
            for user in roles_named['helper'].members:
                if user not in helpers and user not in mods and user not in admins:
                    helpers.append(user)

        if roles_named.get("helpers", None):
            for user in roles_named['helpers'].members:
                if user not in helpers and user not in mods and user not in admins:
                    helpers.append(user)

        for id in staff_roles['helper']:
            role_x = ctx['guild'].get_role(id)
            for user in role_x.members:
                if user not in admins and user not in mods and user not in helpers:
                    helpers.append(user)


        admins_txt = ""
        mods_txt = ""
        helpers_txt = ""

        for user in admins:
            if user.status == discord.Status.dnd:
                admins_txt += f"{status_icons['dnd']} {user.mention}\n"
            elif user.status == discord.Status.online:
                admins_txt += f"{status_icons['online']} {user.mention}\n"
            elif user.status == discord.Status.offline or user.status == discord.Status.invisible:
                admins_txt += f"{status_icons['offline']} {user.mention}\n"
            elif user.status == discord.Status.idle:
                admins_txt += f"{status_icons['away']} {user.mention}\n"

        for user in mods:
            if user.status == discord.Status.dnd:
                mods_txt += f"{status_icons['dnd']} {user.mention}\n"
            elif user.status == discord.Status.online:
                mods_txt += f"{status_icons['online']} {user.mention}\n"
            elif user.status == discord.Status.offline or user.status == discord.Status.invisible:
                mods_txt += f"{status_icons['offline']} {user.mention}\n"
            elif user.status == discord.Status.idle:
                mods_txt += f"{status_icons['away']} {user.mention}\n"

        for user in helpers:
            if user.status == discord.Status.dnd:
                helpers_txt += f"{status_icons['dnd']} {user.mention}\n"
            elif user.status == discord.Status.online:
                helpers_txt += f"{status_icons['online']} {user.mention}\n"
            elif user.status == discord.Status.offline or user.status == discord.Status.invisible:
                helpers_txt += f"{status_icons['offline']} {user.mention}\n"
            elif user.status == discord.Status.idle:
                helpers_txt += f"{status_icons['away']} {user.mention}\n"

        if admins_txt == "":
            admins_txt = "No admins found"

        if mods_txt == "":
            mods_txt = "No moderators found"

        if helpers_txt == "":
            helpers_txt = "No helpers found"

        embed.add_field(name="Admins", value=admins_txt, inline=False)
        embed.add_field(name="Moderators", value=mods_txt, inline=False)
        embed.add_field(name="Helpers", value=helpers_txt, inline=False)
        return await ctx['channel'].send(embed=embed)

class PingMod():
    def __init__(self):
        self.name = "pingmog"
        self.example = "{prefix} pingmod [reason]"
        self.desc = "Ping a random moderator in the server."
        self.permission_requirement = "basic"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        if len(ctx['command_args']) < 0:
            await ctx['channel'].send("Please give a reason for pinging the mod.")
        server_data = database.get_server(ctx['guild'].id)
        if server_data.get(Query().staffroles) == None:
            server_data.insert({"staffroles": {
                    "admin": [],
                    "mod": [],
                    "helper": [],
                }
            })
        roles = ctx['guild'].roles

        roles_named = {}
        staff_roles = server_data.get(Query().staffroles)['staffroles']

        helpers = []
        mods = []
        admins = []


        for role in ctx['guild'].roles:
            roles_named[role.name.lower()] = role

        if roles_named.get("admin", None):
            for user in roles_named.get("admin", None).members:
                    admins.append(user)

        if roles_named.get("admins", None):
            for user in roles_named['admins'].members:
                if user not in admins :
                    admins.append(user)

        if roles_named.get("administrator", None):
            for user in roles_named['administrator'].members:
                if user not in admins:
                    admins.append(user)


        for id in staff_roles['admin']:
            role_x = ctx['guild'].get_role(id)
            for user in role_x.members:
                if user not in admins:
                    admins.append(user)

        if roles_named.get("mod", None):
            for user in roles_named.get("mod", None).members:
                if user not in admins:
                    mods.append(user)

        if roles_named.get("moderator", None):
            for user in roles_named['moderator'].members:
                if user not in mods and user not in admins:
                    mods.append(user)

        if roles_named.get("moderators", None):
            for user in roles_named['moderators'].members:
                if user not in mods and user not in admins:
                    mods.append(user)

        for id in staff_roles['mod']:
            role_x = ctx['guild'].get_role(id)
            for user in role_x.members:
                if user not in admins and user not in mods:
                    mods.append(user)

        if roles_named.get("helper", None):
            for user in roles_named['helper'].members:
                if user not in helpers and user not in mods and user not in admins:
                    helpers.append(user)

        if roles_named.get("helpers", None):
            for user in roles_named['helpers'].members:
                if user not in helpers and user not in mods and user not in admins:
                    helpers.append(user)

        for id in staff_roles['helper']:
            role_x = ctx['guild'].get_role(id)
            for user in role_x.members:
                if user not in admins and user not in mods and user not in helpers:
                    helpers.append(user)



        all_staff = admins + mods + helpers
        reason = " ".join(ctx['command_args'])
        embed = Embed(color=discord.Color.dark_red())

        def do_random(staff_list):
            staff_online = [user.mention for user in staff_list if user.status != discord.Status.invisible or user.status != discord.Status.offline]
            staff_mention = random.choice(staff_online)
            return staff_mention

        staff_mention = do_random(all_staff)
        embed.add_field(name="You were pinged for the following reason:", value=reason)
        embed.set_footer(icon_url=ctx['author'].avatar_url, text=f"Ping initiated by {ctx['author']}")
        await ctx['channel'].send(staff_mention, embed=embed)

class Roles():
    def __init__(self):
        self.name = "roles"
        self.example = "{prefix} roles"
        self.desc = "Gets the current roles on the server\nYou can give it a name and it will look for that role individually"
        self.permission_requirement = "basic"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        embed = Embed(color=discord.Color.dark_orange())
        embed.title = "Roles [Sorted by bottom to top]"
        counter = 0
        role_text_dict = {
            0: ""
        }
        for role in ctx['guild'].roles:
            text = f"{role.mention} | ({role.id})\n"
            if len(text) + len(role_text_dict[counter]) > 1024:

                counter += 1
                role_text_dict[counter] = ""
                role_text_dict[counter] += text
            else:
                role_text_dict[counter] += text

        for text in list(role_text_dict.values()):
            embed.add_field(name="---", value=text)

        await ctx['channel'].send(embed=embed)


class ServerSettings():
    def __init__(self):
        self.name = "serversettings"
        self.example = "{prefix} serversettings"
        self.desc = "Gets the current settings of the server"
        self.permission_requirement = "basic"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        '''run command'''
        embed = Embed(color=discord.Color.dark_blue())
        embed.title = ctx['guild'].name + " Server Settings"
        guild_db = database.get_server(ctx['guild'].id)
        server_settings = {}
        for setting in guild_db.all():
            server_settings.update(setting)

        customcommands_db = ctx['customcoms'].get_server(ctx['guild'].id).all()
        custom_commands = []
        for x in customcommands_db:
            for key in x:
                custom_commands.append(key)

        if 'prefix' in server_settings:
            embed.add_field(name="Current Prefix", value=server_settings['prefix'], inline=False)

        if 'user_join_role' in server_settings:
            embed.add_field(name="User Join Role", value=f"<@&{server_settings['user_join_role']}>", inline=False)

        if 'bot_join_role' in server_settings:
            embed.add_field(name="Bot Join Role", value=f"<@&{server_settings['bot_join_role']}>", inline=False)

        if 'modlog' in server_settings:
            embed.add_field(name="Mod Log Channel", value=f"<#{server_settings['modlog']}>", inline=False)

        if len(mute_timer_handler.get_all_mutes(ctx['guild'].id)) >= 0:
            embed.add_field(name="Current Mutes", value=f"{len(mute_timer_handler.get_all_mutes(ctx['guild'].id))} people muted currently", inline=False)

        if len(custom_commands) >= 1:
            embed.add_field(name="Custom Commands", value=f"```{', '.join(custom_commands)}```", inline=False)


        return await ctx['channel'].send(embed=embed)



class Help():
    '''help commands'''

    def __init__(self):
        self.name = "help"
        self.example = "{prefix} help"
        self.desc = "Gives the command availble for use."
        self.permission_requirement = "basic"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        '''Run command'''

        prefix = database.get_server(ctx['guild'].id).get(Query().prefix)['prefix']
        embed = Embed()
        embed.set_footer(text=f"do {prefix} help <command> for more info")
        embed.add_field(name="Current prefix:", value=prefix, inline=False)

        if(len(ctx['command_args']) == 0):
            embed.add_field(name=f"{prefix} help basic", value="Show all basic commands", inline=False)
            embed.add_field(name=f"{prefix} help moderator", value="Show all moderator commands", inline=False)
            embed.add_field(name=f"{prefix} help admin", value="Show all admin commands", inline=False)
            embed.add_field(name=f"{prefix} help owner", value="Show all owner commands", inline=False)
        elif(ctx['command_args'][0] == "basic"):
            command_names = []
            for x in commands:
                if commands[x].permission_requirement == "basic":
                    command_names.append(commands[x].name)
            embed.add_field(name="Basic Commands:", value=f"```{', '.join(command_names)}```", inline=False)

        elif(ctx['command_args'][0] == "moderator"):
            command_names = []
            for x in commands:
                if commands[x].permission_requirement == "moderator":
                    command_names.append(commands[x].name)
            embed.add_field(name="Moderator Commands:", value=f"```{', '.join(command_names)}```", inline=False)
        elif(ctx['command_args'][0] == "admin"):
            command_names = []
            for x in commands:
                if commands[x].permission_requirement == "admin":
                    command_names.append(commands[x].name)
            embed.add_field(name="Admin Commands:", value=f"```{', '.join(command_names)}```", inline=False)
        elif(ctx['command_args'][0] == "owner"):
            command_names = []
            for x in commands:
                if commands[x].permission_requirement == "owner":
                    command_names.append(commands[x].name)
            embed.add_field(name="Owner Commands:", value=f"```{', '.join(command_names)}```", inline=False)
        elif (ctx['command_args'][0] in commands):
            embed.add_field(name=f"{prefix} {commands[ctx['command_args'][0]].name}", value=f"{commands[ctx['command_args'][0]].desc}\nExample: `{commands[ctx['command_args'][0]].example.format(prefix=prefix)}`", inline=False)
        else:
            embed.add_field(name=f"{prefix} help basic", value="Give all basic commands in a single embed", inline=False)
            embed.add_field(name=f"{prefix} help moderator", value="Give moderator all commands in a single embed", inline=False)
            embed.add_field(name=f"{prefix} help admin", value="Give all admin commands in a single embed", inline=False)
            embed.add_field(name=f"{prefix} help owner", value="Give all owner commands in a single embed", inline=False)

        return await ctx['channel'].send(embed=embed)

class ServerInfo():
    '''Server info command'''
    def __init__(self):
        self.name="serverinfo"
        self.example="{prefix} serverinfo"
        self.desc="Gives an embed containing current server info"
        self.permission_requirement = "basic"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        '''run serverinfo command'''
        botcount = 0
        usercount = 0
        epochtime = ctx['guild'].created_at.timestamp()
        now = time.time()
        dayssince = (now - epochtime) / 86400
        roles = []
        for role in ctx['guild'].roles:
            if role.name != "@everyone":
                roles.append(role.mention)
        for user in ctx['guild'].members:
            if user.bot:
                botcount += 1
            else:
                usercount += 1
        try:
            embed = Embed(colour=0xCC8400)
            embed.add_field(name="Server Name:", value=ctx['guild'].name, inline=False)
            embed.add_field(name="Total Member Count:", value=f"{ctx['guild'].member_count}", inline=False)
            embed.add_field(name="Bot Member Count:", value=f'{botcount}', inline=False)
            embed.add_field(name="Roles:", value=" | ".join(roles))
            embed.set_thumbnail(url=ctx['guild'].icon_url)
            embed.set_footer(text="Guild created: " + str(time.strftime('%d/%m/%Y %H:%M:%S', time.localtime(epochtime)) + " (" + str(round(dayssince)) + " days ago)"))
            await ctx['channel'].send(embed=embed)
        except:
            embed = Embed(colour=0xCC8400)
            embed.add_field(name="Server Name:", value=ctx['guild'].name, inline=False)
            embed.add_field(name="Total Member Count:", value=f"{ctx['guild'].member_count}", inline=False)
            embed.add_field(name="Human Member Count:", value=f'{usercount}', inline=False)
            embed.add_field(name="Bot Member Count:", value=f'{botcount}', inline=False)
            #embed.add_field(name="Roles:", value="".join([x.mention for x in ctx['guild'].roles]))
            embed.set_thumbnail(url=ctx['guild'].icon_url)
            embed.set_footer(text="Guild created: " + str(time.strftime('%d/%m/%Y %H:%M:%S', time.localtime(epochtime)) + " (" + str(round(dayssince)) + " days ago)"))
            await ctx['channel'].send(embed=embed)


class UserInfo():
    '''useinfo command'''
    def __init__(self):
        self.name="userinfo"
        self.example="{prefix} userinfo"
        self.desc="Gives info about a user"
        self.permission_requirement="basic"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def getUserInfo(self, ctx, client, userid):
        '''run userinfo command'''
        usr = await client.fetch_user(userid)
        now = time.time()
        epochtime = usr.created_at.timestamp()
        dayssince = (now - epochtime) / 86400
        e = Embed(colour=0xCC8400)
        e.set_thumbnail(url=usr.avatar_url)
        e.add_field(name="Username:", value=usr.name, inline=True)
        e.add_field(name="User Discriminator:", value=usr.discriminator, inline=True)
        e.add_field(name="User Id:", value=usr.id, inline=True)
        e.add_field(name="Bot Account:", value=str(usr.bot), inline=True)
        e.add_field(name="Created on:", value=str(time.strftime('%d/%m/%Y %H:%M:%S', time.localtime(epochtime)) + " **(" + str(round(dayssince)) + " days ago)**"), inline=False)
        try:
            member = ctx['guild'].get_member(userid)
            jointime = member.joined_at.timestamp()
            dayssincejoin = (now - jointime) / 86400
            roles = []
            for role in member.roles:
                if role.name != "@everyone":
                    roles.append(role.mention)
            e.add_field(name="Joined Server on:", value=str(time.strftime('%d/%m/%Y %H:%M:%S', time.localtime(jointime)) + " **(" + str(round(dayssincejoin)) + " days ago)**"), inline=False)
            e.add_field(name="Roles:", value=" ".join(roles))
        except:
            member = None
            e.add_field(name="Roles:", value="User not in server")
            e.add_field(name="Joined Server on:", value="User not in server", inline=False)





        await ctx['channel'].send(embed=e)

    async def run(self, ctx, client, database):
        if len(ctx['command_args']) == 0:
            await self.getUserInfo(ctx, client, ctx['author'].id)
        else:
            try:
                userid = ctx['command_args'][0].replace("<@", "").replace(">", "").replace("!", "")
                await self.getUserInfo(ctx, client, int(userid))
            except:
                await ctx['channel'].send("User not found.")


class Status():
    '''Status command'''
    def __init__(self):
        self.name="status"
        self.example="{prefix} status"
        self.desc="Gives info about the status of the bot"
        self.permission_requirement="basic"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        '''run status command'''
        shardId = str(ctx['guild'].shard_id + 1)
        shardCount = str(client.shard_count)

        t1 = time.perf_counter()
        await ctx['channel'].trigger_typing()
        t2 = time.perf_counter()
        pingTime = round((t2-t1)*1000)

        e = Embed(color=0xCC8400)
        e.set_author(name="Phoenix", icon_url=client.user.avatar_url)
        e.add_field(name="Guilds:", value=len(client.guilds), inline=True)
        e.add_field(name="Ping:", value="{}ms".format(pingTime), inline=True)
        e.add_field(name="Shard id:", value=shardId + " / " + shardCount, inline=True)
        e.add_field(name="CPU Usage:", value=str(cpupercent) + "%", inline=True)
        e.add_field(name="RAM Usage:", value=str(round(memusage / 1000000)) + "MB", inline=True)
        e.add_field(name="Discord.py version:", value=discord.__version__, inline=True)
        await ctx['channel'].send(embed=e)


class Roll():
    '''roll command'''
    def __init__(self):
        self.name="roll"
        self.example="{prefix} roll [num]"
        self.desc="Rolls a die"
        self.permission_requirement="basic"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        '''run roll command'''
        if len(ctx['command_args']) == 0:
            random_number = randint(1, 6)
            await ctx['channel'].send("Your dice role gave a {}".format(random_number))
        else:
            try:
                random_number = randint(1, int(ctx['command_args'][0]))
                await ctx['channel'].send("Your dice role gave a {}".format(random_number))
            except:
                await ctx['channel'].send("`{}` is not a valid number".format(ctx['command_args'][0]))


class Pat():
    '''Pat command'''
    def __init__(self):
        self.name="pat"
        self.example="{prefix} pat [user]"
        self.desc="Pats a user on the head"
        self.permission_requirement="basic"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        if len(ctx['command_args']) == 0:
            user2 = "Someone"
        else:
            user2 = ctx['command_args'][0]

        user1 = ctx['author'].mention
        e = Embed(color=0xCC8400)
        e.set_image(url="https://media.giphy.com/media/xUA7bahIfcCqC7S4qA/giphy.gif")
        e.add_field(name="Someone just got patted on the head.", value=f"{user1} has pat {user2} on the head.")
        await ctx['channel'].send(embed=e)



class CoinFlip():
    '''Coinflip command'''
    def __init__(self):
        self.name="coinflip"
        self.example="{prefix} coinflip"
        self.desc="Flips a coin"
        self.permission_requirement="basic"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        await ctx['channel'].send("The flipped coin landed on: {}".format(['head', 'tails'][randint(0,1)]))


class Boop():
    '''Boop command'''
    def __init__(self):
        self.name="boop"
        self.example="{prefix} boop [user]"
        self.desc="Boops a user on the nose"
        self.permission_requirement="basic"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        if len(ctx['command_args']) == 0:
            user2 = "Someone"
        else:
            user2 = ctx['command_args'][0]
        user1 = ctx['author'].mention
        e = Embed(color=0xCC8400)
        e.set_image(url="https://media1.giphy.com/media/2dnTHovkLt6Yo/giphy.gif")
        e.add_field(name="Someone just got booped on the nose.", value=f"{user1} has pat {user2} on the head.")
        await ctx['channel'].send(embed=e)

class WeebSh():
    '''Boop command'''
    def __init__(self):
        self.name="weehsh"
        self.example="{prefix} weehsh [tag] [type] [nsfw (yes/no) Default: no]"
        self.desc="Get an image using weeb.sh api"
        self.permission_requirement="basic"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):

        if not ctx['channel'].is_nsfw():
            return await ctx['channel'].send("Please only use this command in NSFW channels")

        try:
            if len(ctx['command_args']) == 0:
                tags = await ctx['weebsh'].get_tags()
                types = await ctx['weebsh'].get_types()
                return await ctx['channel'].send(f"Available Tags:```\n{', '.join(tags)}```Available Types:```\n{', '.join(types)}```")
            elif len(ctx['command_args']) == 1:
                tags = await ctx['weebsh'].get_tags()
                if ctx['command_args'][0] not in tags:
                    return await ctx['channel'].send("That tag is invalid!")
                try:
                    image = await ctx['weebsh'].get_image(tags=[ctx['command_args'][0]], nsfw=False)#
                except:
                    return await ctx['channel'].send("Image not found")
                return await ctx['channel'].send(f"Image URL **(Powered by Weeb.sh)**:\n{image[0]}")
            elif len(ctx['command_args']) == 2:
                types = await ctx['weebsh'].get_types()
                if ctx['command_args'][1] not in types:
                    return await ctx['channel'].send("That image type is invalid!")
                try:
                    image = await ctx['weebsh'].get_image(tags=[ctx['command_args'][0]], imgtype=ctx['command_args'][1], nsfw=False)
                except:
                    return await ctx['channel'].send("Image not found")
                return await ctx['channel'].send(f"Image URL **(Powered by Weeb.sh)**:\n{image[0]}")
            elif len(ctx['command_args']) == 3:
                if ctx['command_args'][2] == "yes":
                    nsfw = "true"
                else:
                    nsfw = "false"
                try:
                    image = await ctx['weebsh'].get_image(tags=[ctx['command_args'][0]], imgtype=ctx['command_args'][1], nsfw=nsfw)
                except:
                    return await ctx['channel'].send("Image not found")
                return await ctx['channel'].send(f"Image URL **(Powered by Weeb.sh)**:\n{image[0]}")
            else:
                return await ctx['channel'].send(f"Incorrect command syntax {self.example}")
        except Exception as e:
            return await ctx['channel'].send("Image not found")

class Danbooru_cmd():

    def __init__(self):
        self.name = "danbooru"
        self.example = "{prefix} danbooru [tags] OR [tag] rating:[e/s/q]"
        self.desc = "Get an image from the danbooru api, locked to NSFW.\n(Ratings: e for explicit, q for questionable, s for (relatively) safe)\nFor more info on tags and rating: [Go here](https://danbooru.donmai.us/wiki_pages/43049#dtext-n2)"
        self.permission_requirement = "basic"
        self._api = Danbooru()
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):

        args = ctx['command_args'][:2]

        if ctx['author'].bot:
        	return

        if len(args) < 1:
        	return

        if not ctx['channel'].is_nsfw():
            return await ctx['channel'].send("Please only use this command in NSFW channels")

        try:
            result = self._api.random_with_tags(*args)
            return await ctx['channel'].send(result)
        except:
            result = self._api.random_with_tags(*args)

            if result == None:
                   return await ctx['channel'].send("Invalid tags!")

            if result == "":
                   return await ctx['channel'].send("Nothing found")
            return await ctx['channel'].send(result)


class ListCustomCommands():
    def __init__(self):
        self.name="listcustomcommands"
        self.example="{prefix} listcustomcommands"
        self.desc="List the available custom commands in the server"
        self.permission_requirement="basic"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        customcommands_db = ctx['customcoms'].get_server(ctx['guild'].id).all()
        custom_commands = []
        for x in customcommands_db:
            for key in x:
                custom_commands.append(key)

        if len(custom_commands) == 0:
            return await ctx['channel'].send("There are no custom commands for this server!")

        return await ctx['channel'].send(f"Available custom commands:\n```{', '.join(custom_commands)}```Use `{ctx['prefix']} [command name]` to use any of these commands")


class RemindMe():
    def __init__(self):
        self.name="remindme"
        self.example="{prefix} remindme <time>[m/h/d] <thing>\n{prefix} remindme 1h Ping!"
        self.desc="Tell the bot to remind you about something"
        self.permission_requirement="basic"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        reminder_db = Database("reminders")
        user_reminders = reminder_db.get_server(ctx['author'].id)
        reminders_current = user_reminders.get(Query().reminders)
        if reminders_current == None:
            user_reminders.insert({'reminders': []})
            reminders_current = user_reminders.get(Query().reminders)['reminders']

        reminders_current = user_reminders.get(Query().reminders)['reminders']

        if len(ctx['command_args']) == 0:
            return await ctx['channel'].send("Please provide a time.")
        if len(ctx['command_args']) == 1:
            return await ctx['channel'].send("Please provide a reason.")

        time_str = "".join([i for i in ctx['command_args'][0] if i.isdigit()])
        if time_str == "":
            time = 0
        else:
            time = int(time_str)
        time_length = "".join([i for i in ctx['command_args'][0] if not i.isdigit()])

        current_time = round(get_time())


        if time == "":
            time = 1

        if time_length == "s" or time_length == "sec" or time_length == "second" or time_length == "seconds":
            if time < 1:
                time_txt = "Second"
            else:
                time_txt = "Seconds"
            time_to_remind = time
        elif time_length == "m" or time_length == "min" or time_length == "minute" or time_length == "minutes":
            if time < 1:
                time_txt = "Minute"
            else:
                time_txt = "Minutes"
            time_to_remind = time * 60
        elif time_length == "h" or time_length == "hr" or time_length == "hour" or time_length == "hours":
            if time < 1:
                time_txt = "Hour"
            else:
                time_txt = "Hours"
            time_to_remind = time * 3600
        elif time_length == "d" or time_length == "day" or time_length == "days":
            if time < 1:
                time_txt = "Day"
            else:
                time_txt = "Days"
            time_to_remind = time * 86400
        else:
            return await ctx['channel'].send(f"""
Invalid Time. Here are some examples:```css
{ctx['prefix']} remindme 1s Ping!
{ctx['prefix']} remindme 1m Ping!
{ctx['prefix']} remindme 1h Ping!
{ctx['prefix']} remindme 1d Ping!
```""")
        reason = " ".join(ctx['command_args'][1:])
        reminder_dict = {"time_set": round(get_time()), "time_until_reminder": time, "reason": reason}
        reminders_current.append(reminder_dict)
        user_reminders.update({"reminders": reminders_current}, Query().reminders)

        reminder_thread.Reminder_Checks(client).start(ctx['author'], reason, time_to_remind, reminder_dict)

        return await ctx['channel'].send(f"Okay. I will remind you in `{time} {time_txt}`\nReminder reason: `{' '.join(ctx['command_args'][1:])}`")

class YoutubeSearch():

    def __init__(self):
        self.name="youtube"
        self.example="{prefix} youtube <query>"
        self.desc="Search youtube"
        self.permission_requirement="basic"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):

        if len(ctx['command_args']) == 0:
            return await ctx['channel'].send("Please provide a query")

        search_query = "%20".join(ctx['command_args'])

        return await ctx['channel'].send(yt.get_search(query=search_query)['results'][0]['video']['url'])

def init_commands():
    Ping()
    Staff()
    PingMod()
    Roles()
    ServerSettings()
    Help()
    ServerInfo()
    UserInfo()
    Status()
    Roll()
    Pat()
    CoinFlip()
    Boop()
    WeebSh()
    Danbooru_cmd()
    ListCustomCommands()
    RemindMe()
    YoutubeSearch()
    Support()
    Suggest()
    ListAliases()
