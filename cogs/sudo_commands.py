'''Sudo commands'''
import subprocess
import sys
import os
import discord
import inspect
import settings
from src.command_registery import COMMANDS as commands
from src import command_registery, database_handler
from io import StringIO
from tinydb import Query
import traceback

class Exec():
    '''Exec command'''
    def __init__(self):
        self.name = "exec"
        self.example = "{prefix} exec [thing]"
        self.desc = "Evaluate code on the bot"
        self.permission_requirement = "owner"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        '''Run eval command'''

        if ctx['author'].id not in settings.eval_users:
            return

        if "settings.token" in ctx['command_args'] :
            return await ctx['channel'].send(f"```py\nResult:\nNot Today Kiddo (╯°□°）╯``````py\nConsole Ouput:\nNone\n```")

        try:

            old_stdout = sys.stdout
            redirect = sys.stdout = StringIO()
            to_strip = (len(ctx['prefix']) + len(ctx['command_name']) + 2)
            code = ctx['_main'].content[to_strip:]

            env = {
                'client': client,
                'commands': commands,
                'ctx': ctx,
                'msg': ctx['_main'],
                'database': database,
                'modlogs': ctx['modlogs'],
                'query': Query
            }

            env.update(globals())

            try:
                result = exec(code, env)
                if inspect.isawaitable(result):
                    result = await result
                if len(redirect.getvalue()) < 1:
                    sys.stdout = old_stdout
                    return await ctx['channel'].send(f"```py\nResult:\n{result}``````py\nConsole Ouput:\nNone\n```")
                await ctx['channel'].send(f"```py\nResult:\n{result}``````py\nConsole Ouput:\n{redirect.getvalue()}\n```")
            except Exception as e:
                if len(redirect.getvalue()) < 1:
                    sys.stdout = old_stdout
                    return await ctx['channel'].send(f"```py\nCode [Result] Error:\n{traceback.format_exc()}``````py\nConsole Ouput:\nNone\n```")
                await ctx['channel'].send(f"```py\nCode [Result] Error:\n{e}``````py\nConsole Ouput:\n{redirect.getvalue()}\n```")

            sys.stdout = old_stdout
        except Exception as e:
            await ctx['channel'].send(f"```{e}```")

class Blacklist():
    def __init__(self):
        self.name = "blacklist"
        self.example = "{prefix} blacklist [user ids]"
        self.desc = "Blacklist a user from the suggest command."
        self.permission_requirement = "owner"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        blacklist_db = database_handler.Database("blacklist").get_server(0)
        if blacklist_db.get(Query().users) == None:
            blacklist_db.insert({"users": []})

        if len(ctx['command_args']) == 0:
            await ctx['channel'].send("Give a user id")

        user_ids = [user.replace("<@", "").replace("!", "").replace(">", "") for user in ctx['command_args']]

        #Checking the users exist
        exist = []
        dont_exist = []
        already_blacklisted = []
        blacklisted = blacklist_db.get(Query().users)['users']
        for user in user_ids:
            try:
                user_fetch = await client.fetch_user(int(user))
            except ValueError:
                dont_exist.append(user)

            if user_fetch == None:
                dont_exist.append(user)
                continue
            else:
                if int(user) not in blacklisted:
                    exist.append(user)
                else:
                    already_blacklisted.append(user)

        blacklisted_new = blacklisted + [int(user) for user in exist]

        blacklist_db.update({'users': blacklisted_new}, Query().users)

        return await ctx['channel'].send(f"**Blacklisted Successfully**: {len(exist)}\n**Users already blacklisted**: {len(already_blacklisted)}\n**Users not found**: {len(dont_exist)}\n**Total blacklisted users**: {len(blacklisted_new)}")

class Unblacklist():
    def __init__(self):
        self.name = "unblacklist"
        self.example = "{prefix} unblacklist [user ids]"
        self.desc = "Unblacklist a user from the suggest command."
        self.permission_requirement = "owner"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        blacklist_db = database_handler.Database("blacklist").get_server(0)
        if blacklist_db.get(Query().users) == None:
            blacklist_db.insert({"users": []})

        if len(ctx['command_args']) == 0:
            await ctx['channel'].send("Give a user id")

        user_ids = [user.replace("<@", "").replace("!", "").replace(">", "") for user in ctx['command_args']]

        #Checking the users exist
        exist = []
        dont_exist = []
        not_blacklisted = []
        blacklisted = blacklist_db.get(Query().users)['users']
        for user in user_ids:
            try:
                user_fetch = await client.fetch_user(int(user))
            except ValueError:
                dont_exist.append(user)

            if user_fetch == None:
                dont_exist.append(user)
                continue
            else:
                if int(user) in blacklisted:
                    exist.append(user)
                else:
                    not_blacklisted.append(user)

        for user in exist:
            blacklisted.remove(int(user))

        blacklist_db.update({'users': blacklisted}, Query().users)

        return await ctx['channel'].send(f"**Unblacklisted Successfully**: {len(exist)}\n**Users not blacklisted**: {len(not_blacklisted)}\n**Users not found**: {len(dont_exist)}\n**Total blacklisted users**: {len(blacklisted)}")



class Eval():
    '''Eval command'''
    def __init__(self):
        self.name = "eval"
        self.example = "{prefix} eval [thing]"
        self.desc = "Evaluate code on the bot"
        self.permission_requirement = "owner"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        '''Run eval command'''

        if ctx['author'].id not in settings.eval_users:
            return

        if "settings.token" in ctx['command_args'] :
            return await ctx['channel'].send(f"```py\nResult:\nNot Today Kiddo (╯°□°）╯``````py\nConsole Ouput:\nNone\n```")

        try:

            old_stdout = sys.stdout
            redirect = sys.stdout = StringIO()
            to_strip = (len(ctx['prefix']) + len(ctx['command_name']) + 2)
            code = ctx['_main'].content[to_strip:]

            env = {
                'client': client,
                'commands': commands,
                'ctx': ctx,
                'msg': ctx['_main'],
                'database': database,
                'modlogs': ctx['modlogs'],
                'query': Query
            }

            env.update(globals())

            try:
                result = eval(code, env)
                if inspect.isawaitable(result):
                    result = await result
                if len(redirect.getvalue()) < 1:
                    sys.stdout = old_stdout
                    return await ctx['channel'].send(f"```py\nResult:\n{result}``````py\nConsole Ouput:\nNone\n```")
                await ctx['channel'].send(f"```py\nResult:\n{result}``````py\nConsole Ouput:\n{redirect.getvalue()}\n```")
            except Exception as e:
                if len(redirect.getvalue()) < 1:
                    sys.stdout = old_stdout
                    return await ctx['channel'].send(f"```py\nCode [Result] Error:\n{traceback.format_exc()}``````py\nConsole Ouput:\nNone\n```")
                await ctx['channel'].send(f"```py\nCode [Result] Error:\n{e}``````py\nConsole Ouput:\n{redirect.getvalue()}\n```")

            sys.stdout = old_stdout
        except Exception as e:
            await ctx['channel'].send(f"```{e}```")

class Restart():
    '''Restart commands'''
    def __init__(self):
        self.name = "restart"
        self.example = "{prefix} restart"
        self.desc = "Restart the bot"
        self.permission_requirement = "owner"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        '''Run restart command'''
        if ctx['author'].id not in settings.eval_users:
            return

        try:
            game = discord.Game("RESTARTING")
            await client.change_presence(status=discord.Status.invisible, activity=game)
            os.execv(f'{os.getcwd()}/app.py', ['py'])
            await client.logout()
        except Exception as e:
            print(e)
            await ctx['channel'].send("There was an error restarting the bot.")

class Update():
    '''Update command'''
    def __init__(self):
        self.name = "update"
        self.example = "{prefix} update"
        self.desc = "Update the bot via `git pull`"
        self.permission_requirement = "owner"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        '''Run update command'''
        if ctx['author'].id in settings.eval_users:
            message = await ctx['channel'].send("Updating...")
            func = 'git pull'.replace("`", "").split()
            command_run = subprocess.Popen(func, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            output = command_run.stdout.read().decode()
            desc = "```css\n{0}```".format(output)
            em = discord.Embed(color=0xCC8400)
            em.set_author(name=client.user.name, url="https://fanaticphoenix.co.uk")
            em.set_footer(text=f"Phoenix | By Luke#6723")
            em.set_thumbnail(url=client.user.avatar_url)
            em.add_field(name="Update Complete.", value=desc)
            await message.edit(content=None, embed=em)

def init_commands():
    Update()
    Eval()
    Exec()
    Restart()
    Blacklist()
    Unblacklist()
