'''Admin command classes'''
from tinydb import Query
from tinydb.operations import delete
import settings
from src.check_permissions import check_permissions as perms
from src.command_registery import COMMANDS as commands
from src import command_registery
import datetime
import discord

def get_prefix(server_id, database):
    '''Get prefix based on server id'''
    server_data = database.get_server(server_id)
    return server_data.get(Query().prefix)['prefix']


class AddCommandAlias():
    def __init__(self):
        self.name = "addcommandalias"
        self.example = "{prefix} addcommandalias <command> <newcommandname>"
        self.desc = "Add a command alias for your server"
        self.permission_requirement = "admin"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        if len(ctx['command_args']) == 0 or len(ctx['command_args']) == 1:
            if len(ctx['command_args']) == 0:
                return await ctx['channel'].send("Please give the command you want to have an alias for.")
            elif len(ctx['command_args']) == 1:
                return await ctx['channel'].send("Please give the name for the alias.")

        server_db = database.get_server(ctx['guild'].id)
        if server_db.get(Query().aliases) == None:
            server_db.insert({'aliases': {}})
        aliases = server_db.get(Query().aliases)['aliases']

        command_alias_name = ctx['command_args'][1]
        command_alias_for = ctx['command_args'][0]

        if command_alias_for not in commands:
            return await ctx['channel'].send("The command you are trying to make an alias for does not exist.")

        if command_alias_name in aliases:
            return await ctx['channel'].send(f"The command alias `{command_alias_name}` already exists for the command `{commands[aliases[command_alias_name]].name}`")

        aliases[command_alias_name] = commands[command_alias_for].name

        server_db.update({'aliases': aliases}, Query().aliases)

        return await ctx['channel'].send("Alias successfully added!")


class DelCommandAlias():
    def __init__(self):
        self.name = "delcommandalias"
        self.example = "{prefix} delcommandalias <aliasname>"
        self.desc = "Remove a command alias for your server"
        self.permission_requirement = "admin"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        if ctx['author'].id in settings.eval_users:
            pass
        elif perms(ctx['_main'], {'manage_messages': False}):
            return await ctx['channel'].send("You do not have permission to do that.")

        if len(ctx['command_args']) == 0:
            return await ctx['channel'].send("Please give the alias you want to remove")

        server_db = database.get_server(ctx['guild'].id)
        if server_db.get(Query().aliases) == None:
            server_db.insert({'aliases': {}})
        aliases = server_db.get(Query().aliases)['aliases']

        command_alias_name = ctx['command_args'][0]

        if command_alias_name not in aliases:
            return await ctx['channel'].send(f"The command alias {aliases[command_alias_name]} doesnt exist`")

        del aliases[command_alias_name]

        server_db.update({'aliases': aliases}, Query().aliases)
        return await ctx['channel'].send("Removed command alias!")


class AddStaffRole():
    '''addstaffrole command'''
    def __init__(self):
        self.name = "addstaffrole"
        self.example = "{prefix} addstaffrole [roletype] <roleid / name>\nRole types: admin, mod, helper"
        self.desc = "Add a staff role by ID or name"
        self.permission_requirement = "admin"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        '''run command'''
        server_data = database.get_server(ctx['guild'].id)
        if ctx['author'].id in settings.eval_users:
            pass
        elif perms(ctx['_main'], {'manage_messages': False}):
            return await ctx['channel'].send("You do not have permission to do that.")
        if len(ctx['command_args']) > 0:
            role_type = ctx['command_args'][0]
            if role_type.lower() not in ["admin", "mod", "helper"]:
                await ctx['channel'].send("Please give a valid role type:\nRole types: **admin, mod, helper**")
            else:
                if len(ctx['command_args']) > 1:
                    role_to_add = None
                    _role = None
                    for role in ctx['guild'].roles:
                        if str(role.id) == ctx['command_args'][1].replace("<@&", "").replace(">", ""):
                            role_to_add = role.id
                            _role = role
                        elif role.name.lower() == " ".join(ctx['command_args'][1:]).lower():
                            role_to_add = role.id
                            _role = role
                    if role_to_add == None:
                        await ctx['channel'].send("That role could not be found")
                    else:
                        if server_data.get(Query().staffroles) == None:
                            server_data.insert({"staffroles": {
                                    "admin": [],
                                    "mod": [],
                                    "helper": [],
                                }
                            })

                        old_dict = server_data.get(Query().staffroles)['staffroles']
                        old_list = old_dict[role_type.lower()]
                        if role_to_add not in old_list:
                            old_list.append(role_to_add)
                        else:
                            return await ctx['channel'].send("Role already in this list!")
                        server_data.update({"staffroles": old_dict}, Query().staffroles)
                        await ctx['channel'].send(f"`{_role.name} added to {role_type.upper()} staff list.`")
                else:
                    await ctx['channel'].send("Please give a role name or id.")
        else:
            await ctx['channel'].send("Please give a role type.")

class RemoveStaffRole():
    '''addstaffrole command'''
    def __init__(self):
        self.name = "removestaffrole"
        self.example = "{prefix} removestaffrole [roletype] <roleid / name>\nRole types: admin, mod, helper"
        self.desc = "Remove a staff role by ID"
        self.permission_requirement = "admin"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        '''run command'''
        server_data = database.get_server(ctx['guild'].id)
        if ctx['author'].id in settings.eval_users:
            pass
        elif perms(ctx['_main'], {'manage_messages': False}):
            return await ctx['channel'].send("You do not have permission to do that.")
        if len(ctx['command_args']) > 0:
            role_type = ctx['command_args'][0]
            if role_type.lower() not in ["admin", "mod", "helper"]:
                await ctx['channel'].send("Please give a valid role type:\nRole types: **admin, mod, helper**")
            else:
                if len(ctx['command_args']) > 1:
                    role_to_remove = None
                    _role = None
                    send_name = None
                    for role in ctx['guild'].roles:
                        if str(role.id) == ctx['command_args'][1].replace("<@&", "").replace(">", ""):
                            role_to_remove = role.id
                            _role = role
                        elif role.name.lower() == " ".join(ctx['command_args'][1:]).lower():
                            role_to_remove = role.id
                            _role = role
                    if role_to_remove == None:
                        send_name = False
                    else:
                        send_name = True
                    if server_data.get(Query().staffroles) == None:
                        server_data.insert({"staffroles": {
                                "admin": [],
                                "mod": [],
                                "helper": [],
                            }
                        })
                    old_dict = server_data.get(Query().staffroles)['staffroles']
                    old_list = old_dict[role_type.lower()]
                    if role_to_remove == None and send_name == False:
                        try:
                            role_to_remove = int(ctx['command_args'][1])
                        except:
                            return await ctx['channel'].send("Role is not in the list!")

                    if role_to_remove in old_list:
                        old_list.remove(role_to_remove)
                    else:
                        return await ctx['channel'].send("Role is not in the list!")
                    server_data.update({"staffroles": old_dict}, Query().staffroles)
                    if send_name:
                        await ctx['channel'].send(f"`{_role.name} removed from {role_type.upper()} staff list.`")
                    else:
                        await ctx['channel'].send(f"`{role_to_remove} removed from {role_type.upper()} staff list.`")
                else:
                    await ctx['channel'].send("Please give a role name or id.")
        else:
            await ctx['channel'].send("Please give a role type.")

class Prefix():
    '''prefix command'''
    def __init__(self):
        self.name = "prefix"
        self.example = "{prefix} prefix [new prefix]"
        self.desc = "Allows you to change the prefix for the current server"
        self.permission_requirement = "admin"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        '''Run command'''

        server_data = database.get_server(ctx['guild'].id)

        if len(ctx['command_args']) == 0:
            return await ctx['channel'].send("Current prefix: `{currentprefix}`"
                                       .format(currentprefix=get_prefix(ctx['guild'].id, database)))

        if ctx['author'].id in settings.eval_users:
            pass
        elif perms(ctx['_main'], {'manage_messages': False}):
            return await ctx['channel'].send("You do not have permission to do that.")

        if server_data.get(Query().prefix) not in [None, "!ph"]:
            server_data.update({'prefix': " ".join(ctx['command_args'])}, Query().prefix)
            return await ctx['channel'].send("Prefix updated to `{newprefix}`"
                                .format(newprefix=" ".join(ctx['command_args'])))
        else:
            server_data.insert({'prefix': " ".join(ctx['command_args'])})
            return await ctx['channel'].send("Prefix set to {newprefix}"
                                .format(newprefix=" ".join(ctx['command_args'])))

class StaffRoles():
    '''staffroles command'''
    def __init__(self):
        self.name = "staffroles"
        self.example = "{prefix} staffroles"
        self.desc = "Shows the custom staff roles for this server"
        self.permission_requirement = "admin"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        '''Run command'''
        server_data = database.get_server(ctx['guild'].id)
        staff_roles = server_data.get(Query().staffroles)['staffroles']

        embed = discord.Embed(color=discord.Color.dark_orange())
        admin_txt = ""
        moderator_txt = ""
        helper_txt = ""

        for id in staff_roles['admin']:
            role = ctx['guild'].get_role(id)
            if role == None:
                admin_txt += f"[ROLE DELETED] ({id})\n"
            else:
                admin_txt += f"[{role.mention}] ({id})\n"

        for id in staff_roles['mod']:
            role = ctx['guild'].get_role(id)
            if role == None:
                moderator_txt += f"[ROLE DELETED] ({id})\n"
            else:
                moderator_txt += f"[{role.mention}] ({id})\n"

        for id in staff_roles['helper']:
            role = ctx['guild'].get_role(id)
            if role == None:
                helper_text += f"[ROLE DELETED] ({id})\n"
            else:
                helper_text += f"[{role.mention}] ({id})\n"

        if admin_txt == "":
            admin_txt = "No roles found"
        if moderator_txt == "":
            moderator_txt = "No roles found"
        if helper_txt == "":
            helper_txt = "No roles found"

        embed.add_field(name="Admin Roles", value=admin_txt, inline=False)
        embed.add_field(name="Moderator Roles", value=moderator_txt, inline=False)
        embed.add_field(name="Helper Roles", value=helper_txt, inline=False)

        await ctx['channel'].send(embed=embed)


class ModLog():
    '''Mod log command'''
    def __init__(self):
        self.name = "modlog"
        self.example = "{prefix} modlog [channel]\n{prefix} modlog disable\n{prefix} modlog reset"
        self.desc = "Set the mod log channel for the server"
        self.permission_requirement = "admin"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        server_data = database.get_server(ctx['guild'].id)
        if ctx['author'].id in settings.eval_users:
            pass
        elif perms(ctx['_main'], {'manage_messages': False}):
            return await ctx['channel'].send("You do not have permission to do that.")

        if len(ctx['command_args']) == 0:
            return await ctx['channel'].send("Please provide a channel")
        else:
            channel_id = ctx['command_args'][0].replace("<#", "").replace(">", "")
            try:
                channel_get = ctx['guild'].get_channel(int(channel_id))
            except:
                pass
            if channel_id == "disable":
                server_data.update({
                    'modlog': ''
                }, Query().modlog)
                return await ctx['channel'].send("Mod log channel disabled")
            if channel_id == "reset":
                times_deleted = 0
                messages = []
                for thing in ctx['modlogs'].get_server(ctx['guild'].id):
                    if server_data.get(Query().modlog) == None:
                        return await ctx['channel'].send("There is no modlog on this server.")
                    else:
                        channel_to_get = server_data.get(Query().modlog)['modlog']
                        channel = ctx['guild'].get_channel(int(channel_to_get))
                        try:
                            message = await channel.fetch_message(thing['message_id'])
                            if (datetime.datetime.utcnow().timestamp() - message.created_at.timestamp()) > 1209600:
                                await message.delete()
                                continue
                        except:
                            continue
                        if len(messages) == 100:
                            try:
                                await channel.delete_messages(messages, bulk)
                                times_deleted += 1
                                messages = []
                            except:
                                messages = []
                        messages.append(message)
                if len(messages) > 0 or times_deleted > 0:
                    try:
                        await channel.delete_messages(messages)
                    except:
                        pass
                    ctx['modlogs'].get_server(ctx['guild'].id).purge()
                    await ctx['channel'].send("ModLog reset complete.\n**The Database has been cleared so running this command again will not work**")
                else:
                    ctx['modlogs'].get_server(ctx['guild'].id).purge()
                    await ctx['channel'].send("Modlog Reset failed\n**Please make sure you haven't ran this command twice before reporting an issue.**")




            elif channel_get == None:
                return await ctx['channel'].send("Please provide a valid channel")
            else:
                if server_data.get(Query().modlog) == None:
                    server_data.insert({
                        'modlog': f'{channel_id}'
                    })
                    return await ctx['channel'].send(f"Mod log channel set to: `{channel_id}`")
                else:
                    server_data.update({
                        'modlog': f'{channel_id}'
                    }, Query().modlog)
                    return await ctx['channel'].send(f"Mod log channel updated to: `{channel_id}`")

class AddCommand():
    def __init__(self):
        self.name = "addcustomcommand"
        self.example = "{prefix} addcommand [commandname] [command text]"
        self.desc = "Add a custom text command to your server"
        self.permission_requirement = "admin"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        if ctx['author'].id in settings.eval_users:
            pass
        elif perms(ctx['_main'], {'manage_messages': False}):
            return await ctx['channel'].send("You do not have permission to do that.")

        if len(ctx['command_args']) <= 1:
            return await ctx['channel'].send("Please use this syntax:\n`" + ctx['prefix'] + " addcommand [cmdname] [output]`")

        server_commands = ctx['customcoms'].get_server(ctx['guild'].id)
        to_strip = (len(ctx['prefix']) + len(ctx['command_name']) + len(ctx['command_args'][0]) + 3)
        command_output = ctx['_main'].content[to_strip:]
        commandname = ctx['command_args'][0]

        if commandname in commands:
            return await ctx['channel'].send("That commands name is taken by a feature already provided by the bot")
        if server_commands.get(Query()[commandname]) == None:
            server_commands.insert({commandname: command_output})
            return await ctx['channel'].send(f"`{commandname}` command created with text output: `{command_output}`")
        else:
            return await ctx['channel'].send("That custom command already exists")

class EditCommand():
    def __init__(self):
        self.name = "editcustomcommand"
        self.example = "{prefix} edit [commandname] [updatedcommand text]"
        self.desc = "Update a custom text command to your server"
        self.permission_requirement = "admin"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        if ctx['author'].id in settings.eval_users:
            pass
        elif perms(ctx['_main'], {'manage_messages': False}):
            return await ctx['channel'].send("You do not have permission to do that.")

        server_commands = ctx['customcoms'].get_server(ctx['guild'].id)
        if len(ctx['command_args']) == 0:
            return await ctx['channel'].send("Please provide a command name.")
        if len(ctx['command_args']) == 1:
            return await ctx['channel'].send("Please provide a command output.")

        commandname = ctx['command_args'][0]
        to_strip = (len(ctx['prefix']) + len(ctx['command_name']) + len(ctx['command_args'][0]) + 3)
        command_output = ctx['_main'].content[to_strip:]

        if server_commands.get(Query()[commandname]) != None:
            server_commands.update({commandname: command_output}, Query()[commandname])
            return await ctx['channel'].send(f"`{commandname}` command updated with new text: `{command_output}`")
        else:
            return await ctx['channel'].send("That custom command doesn't exist")


class DelCommand():
    def __init__(self):
        self.name = "delcustomcommand"
        self.example = "{prefix} delcommand [commandname]"
        self.desc = "Add a custom text command to your server"
        self.permission_requirement = "admin"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):
        if ctx['author'].id in settings.eval_users:
            pass
        elif perms(ctx['_main'], {'manage_messages': False}):
            return await ctx['channel'].send("You do not have permission to do that.")

        server_commands = ctx['customcoms'].get_server(ctx['guild'].id)
        if len(ctx['command_args']) == 0:
            return await ctx['channel'].send("Please provide a command name.")
        commandname = ctx['command_args'][0]
        command_doc_id = server_commands.search(Query()[commandname])[0].doc_id

        if server_commands.get(Query()[commandname]) != None:
            server_commands.remove(doc_ids=[command_doc_id])
            return await ctx['channel'].send(f"`{commandname}` removed")
        else:
            return await ctx['channel'].send("That custom command doesn't exist")

class UserJoinRole():

    def __init__(self):
        self.name = "userroinrole"
        self.example = "{prefix} UserJoinRole [ROLE NAME / ROLE ID / ROLE MENTION]"
        self.desc = "Set the role a user gets on join."
        self.permission_requirement = "admin"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):

        if ctx['author'].id in settings.eval_users:
            pass
        elif perms(ctx['_main'], {'manage_messages': False}):
            return await ctx['channel'].send("You do not have permission to do that.")

        if len(ctx['command_args']) == 0:
            return await ctx['channel'].send("Please give a role mention, name or id")

        role_to_get = ctx['command_args'][0]

        role_search = [x for x in ctx['guild'].roles if x.name.lower() == role_to_get.lower()]

        if len(role_search) == 0:

            role_to_get = role_to_get.replace("<@&", "").replace(">", "")
            final_role_search = [x for x in ctx['guild'].roles if str(x.id) == role_to_get]
            if len(final_role_search) == 0:
                return await ctx['channel'].send("That role doesn't exist")
            else:
                role_search = final_role_search

        role = role_search[0]

        guild_db = database.get_server(ctx['guild'].id)

        if guild_db.get(Query().user_join_role) == None:
            guild_db.insert({'user_join_role': f"{role.id}"})
        else:
            guild_db.update({'user_join_role': f"{role.id}"}, Query().user_join_role)

        return await ctx['channel'].send(f"Users join role has been set to: `{role.name} ({role.id})`")

class BotJoinRole():

    def __init__(self):
        self.name = "botjoinrole"
        self.example = "{prefix} botjoinrole [ROLE NAME / ROLE ID / ROLE MENTION]"
        self.desc = "Set the role the bots get on join."
        self.permission_requirement = "admin"
        command_registery.register_command(commandname=self.name, command_class=self)

    async def run(self, ctx, client, database):

        if ctx['author'].id in settings.eval_users:
            pass
        elif perms(ctx['_main'], {'manage_messages': False}):
            return await ctx['channel'].send("You do not have permission to do that.")

        if len(ctx['command_args']) == 0:
            return await ctx['channel'].send("Please give a role mention, name or id")

        role_to_get = ctx['command_args'][0]

        role_search = [x for x in ctx['guild'].roles if x.name.lower() == role_to_get.lower()]

        if len(role_search) == 0:

            role_to_get = role_to_get.replace("<@&", "").replace(">", "")
            final_role_search = [x for x in ctx['guild'].roles if str(x.id) == role_to_get]
            if len(final_role_search) == 0:
                return await ctx['channel'].send("That role doesn't exist")
            else:
                role_search = final_role_search

        role = role_search[0]

        guild_db = database.get_server(ctx['guild'].id)

        if guild_db.get(Query().bot_join_role) == None:
            guild_db.insert({'bot_join_role': f"{role.id}"})
        else:
            guild_db.update({'bot_join_role': f"{role.id}"}, Query().bot_join_role)

        return await ctx['channel'].send(f"Bots join role has been set to: `{role.name} ({role.id})`")

def init_commands():
    AddStaffRole()
    RemoveStaffRole()
    Prefix()
    StaffRoles()
    ModLog()
    AddCommand()
    EditCommand()
    DelCommand()
    UserJoinRole()
    BotJoinRole()
    AddCommandAlias()
    DelCommandAlias()
