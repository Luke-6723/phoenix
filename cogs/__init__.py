'''Init file for all command assignments'''

from . import basic_commands
from . import moderator_commands
from . import admin_commands
from . import sudo_commands

basic_commands.init_commands()
moderator_commands.init_commands()
admin_commands.init_commands()
sudo_commands.init_commands()
