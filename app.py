#!/usr/bin/python3
'''Importing required Modules'''
import re
import inspect
import discord
import settings
from src import error_handler, database_handler, mute_timer_handler, reminder_startup_handler, unban_startup_handler, unmute_startup_handler
from src.check_permissions import check_permissions_channel
import cogs
from cogs import *
from tinydb import Query
import weeb
from time import time as get_time
import datetime
from threaded import reminder_thread, unban_thread, unmute_thread
from src.command_registery import COMMANDS as commands
from src import command_registery as COMMAND_REGISTERY

sh_client = weeb.client.Client(token=settings.api_keys['weebsh'], user_agent="Phoenix2672/v2")

CLIENT = discord.AutoShardedClient(shards=1, shard_id=1)
DATABASE = database_handler.Database('database')
MODLOG = database_handler.Database('modlogs')
CUSTOMCOMMANDS = database_handler.Database('customcommands')

@CLIENT.event
async def on_ready():
    '''On ready Event Handler for when the bot starts'''
    game = discord.Game("!ph help | Online!")
    await CLIENT.change_presence(status=discord.Status.dnd, activity=game, afk=False)
    print("[Log] Phoenix is Online!")
    await reminder_startup_handler.check_reminders_startup(CLIENT)
    await unban_startup_handler.check_unbans_startup(CLIENT)
    await unmute_startup_handler.check_mutes_startup(CLIENT)

@CLIENT.event
async def on_member_join(member):
    guild = member.guild
    guild_db = DATABASE.get_server(guild.id)
    try:
        if member.bot:
            join_role = guild_db.get(Query().bot_join_role)['bot_join_role']
        else:
            join_role = guild_db.get(Query().user_join_role)['user_join_role']
    except:
        join_role = None

    mutes = mute_timer_handler.get_all_mutes(guild.id)

    if len(mutes) != 0:
        users_muted = [list(x.keys())[0] for x in mute_timer_handler.get_all_mutes(guild.id)]
        if str(member.id) in users_muted:
            roles = guild.roles
            if any([x.name.lower() == "muted" for x in roles]):
                mute_role = [x for x in guild.roles if x.name.lower() == "muted"][0]
                if mute_role not in member.roles:
                    await member.add_roles(mute_role, reason="[Anti-mute evade] Remute")


    else:
        pass

    try:
        if join_role != None:
            guild_role = guild.get_role(int(join_role))
            await member.add_roles(guild_role, reason=f"[{CLIENT.user}] Auto Role")
    except:
        return

@CLIENT.event
async def on_member_remove(user):
    guild = user.guild

    if DATABASE.get_server(guild.id).get(Query().modlog) == None:
        return
    elif DATABASE.get_server(guild.id).get(Query().modlog)['modlog'] == "":
        return
    prefix = "!ph"
    is_last_audit_log_me = False
    audit_moderator = None
    audit_reason = None
    audit_target = None
    audit = None
    audit_created_at = None
    time_now = int(datetime.datetime.utcnow().timestamp())

    if DATABASE.get_server(guild.id).get(Query().prefix) != None:
        prefix = DATABASE.get_server(guild.id).get(Query().prefix)['prefix']

    try:
        async for entry in guild.audit_logs(limit=1, action=discord.AuditLogAction.kick):
            if entry.user == guild.me:
                is_last_audit_log_me = True
            else:
                audit_moderator = entry.user
                audit_reason = entry.reason
                audit_target = entry.target
                audit_created_at = int(entry.created_at.timestamp())
                audit = entry
    except Exception as e:
        try:
            if e.status == 403:
                return
        except:
            print(e)

    if audit_created_at == time_now:
        if is_last_audit_log_me:
            return

        mod_log_db = MODLOG.get_server(guild.id)
        case_number = len(mod_log_db.all()) + 1

        if audit_reason == None:
            audit_reason = f"Please use `{prefix} reason {case_number} [reason]`"

        channel_to_send_to = guild.get_channel(int(DATABASE.get_server(guild.id).get(Query().modlog)['modlog']))

        embed = discord.Embed(colour=discord.Color.dark_red())

        embed.title = f"User Kicked"
        embed.add_field(name="User:", value=f"{user.name}#{user.discriminator}")
        embed.add_field(name="Reason:", value=f"{audit_reason}")
        embed.add_field(name="Moderator:", value=f"{audit_moderator}")
        embed.add_field(name="Case Number", value=f"{case_number}")

        message = await channel_to_send_to.send(embed=embed)

        mod_log_db.insert({
            "case": case_number,
            "reason": audit_reason,
            "userid": user.id,
            "operation": "kick",
            "moderator": audit_moderator.id,
            "message_id": message.id
        })


@CLIENT.event
async def on_member_update(before, after):
    user = before
    user_id = user.id
    guild = before.guild
    if "muted" in [role.name.lower() for role in before.roles] and "muted" in [role.name.lower() for role in after.roles]:
        return
    if "muted" not in [role.name.lower() for role in before.roles] and "muted" not in [role.name.lower() for role in after.roles]:
        return
    if DATABASE.get_server(guild.id).get(Query().modlog) == None:
        return
    elif DATABASE.get_server(guild.id).get(Query().modlog)['modlog'] == "":
        return
    else:
        prefix = "!ph"
        is_last_audit_log_me = False
        audit_moderator = None
        audit_reason = None
        audit_roles_before = []
        audit_roles_after = []
        audit_created_at = None
        time_now = int(datetime.datetime.utcnow().timestamp())

        if DATABASE.get_server(guild.id).get(Query().prefix) != None:
            prefix = DATABASE.get_server(guild.id).get(Query().prefix)['prefix']
        try:
            async for entry in guild.audit_logs(limit=1, action=discord.AuditLogAction.member_role_update):
                if entry.user == guild.me:
                    is_last_audit_log_me = True
                else:
                    audit_moderator = entry.user
                    audit_reason = entry.reason
                    audit_roles_before = [role.name.lower() for role in entry.before.roles]
                    audit_roles_after = [role.name.lower() for role in entry.after.roles]
                    audit_created_at = int(entry.created_at.timestamp())
        except Exception as e:
            try:
                if e.status == 403:
                    return
            except:
                return

        if time_now != audit_created_at:
            return

        if "muted" in audit_roles_before and "muted" not in audit_roles_after:
            embed = discord.Embed(colour=discord.Color.green())
            operation = "unmute"
            operation_txt = "Unmuted"
            mutes = database_handler.Database('server_mutes').get_server(guild.id)
            mute_list = mutes.get(Query().mutes)['mutes']
            if mutes.get(Query().mutes) == None:
                mutes.insert({'mutes': []})
            for mute in mute_list:
                if str(user.id) in mute.keys():
                    mute_list.remove(mute)

            mutes.update({'mutes': mute_list}, Query().mutes)
        elif "muted" not in audit_roles_before and "muted" in audit_roles_after:
            embed = discord.Embed(colour=discord.Color.dark_orange())
            operation = "mute"
            operation_txt = "Muted"
            mute_timer_handler.add_mute(guild.id, user_id, -1)
        elif "muted" in audit_roles_after and "muted" in audit_roles_before or "muted" not in audit_roles_after and "muted" not in audit_roles_before:
            return




        if is_last_audit_log_me:
            return

        mod_log_db = MODLOG.get_server(guild.id)
        case_number = len(mod_log_db.all()) + 1

        if audit_reason == None:
            audit_reason = f"Please use `{prefix} reason {case_number} [reason]`"

        channel_to_send_to = guild.get_channel(int(DATABASE.get_server(guild.id).get(Query().modlog)['modlog']))

        embed.title = f"User {operation_txt}"
        embed.add_field(name="User:", value=f"{user.name}#{user.discriminator}")
        embed.add_field(name="Reason:", value=f"{audit_reason}")
        embed.add_field(name="Moderator:", value=f"{audit_moderator}")
        embed.add_field(name="Case Number", value=f"{case_number}")

        message = await channel_to_send_to.send(embed=embed)

        mod_log_db.insert({
            "case": case_number,
            "reason": audit_reason,
            "userid": user.id,
            "operation": f"{operation}",
            "moderator": audit_moderator.id,
            "message_id": message.id
        })



@CLIENT.event
async def on_member_ban(guild, user):

    if DATABASE.get_server(guild.id).get(Query().modlog) == None:
        return
    elif DATABASE.get_server(guild.id).get(Query().modlog)['modlog'] == "":
        return

    prefix = "!ph"
    is_last_audit_log_me = False
    audit_moderator = None
    audit_reason = None

    if DATABASE.get_server(guild.id).get(Query().prefix) != None:
        prefix = DATABASE.get_server(guild.id).get(Query().prefix)['prefix']

    try:
        async for entry in guild.audit_logs(limit=1, action=discord.AuditLogAction.ban):
            if entry.user == guild.me:
                is_last_audit_log_me = True
            else:
                audit_moderator = entry.user
                audit_reason = entry.reason
    except Exception as e:
        try:
            if e.status == 403:
                return
        except:
            return




    if is_last_audit_log_me:
        return

    mod_log_db = MODLOG.get_server(guild.id)
    case_number = len(mod_log_db.all()) + 1

    if audit_reason == None:
        audit_reason = f"Please use `{prefix} reason {case_number} [reason]`"

    channel_to_send_to = guild.get_channel(int(DATABASE.get_server(guild.id).get(Query().modlog)['modlog']))

    embed = discord.Embed(colour=discord.Color.dark_red())

    embed.title = f"User Banned"
    embed.add_field(name="User:", value=f"{user.name}#{user.discriminator}")
    embed.add_field(name="Reason:", value=f"{audit_reason}")
    embed.add_field(name="Moderator:", value=f"{audit_moderator}")
    embed.add_field(name="Case Number", value=f"{case_number}")

    message = await channel_to_send_to.send(embed=embed)

    mod_log_db.insert({
        "case": case_number,
        "reason": audit_reason,
        "userid": user.id,
        "operation": "ban",
        "moderator": audit_moderator.id,
        "message_id": message.id
    })

@CLIENT.event
async def on_member_unban(guild, user):

    if DATABASE.get_server(guild.id).get(Query().modlog) == None:
        return
    elif DATABASE.get_server(guild.id).get(Query().modlog)['modlog'] == "":
        return

    prefix = "!ph"
    is_last_audit_log_me = False
    audit_moderator = None
    audit_reason = None

    if DATABASE.get_server(guild.id).get(Query().prefix) != None:
        prefix = DATABASE.get_server(guild.id).get(Query().prefix)['prefix']

    try:
        async for entry in guild.audit_logs(limit=1, action=discord.AuditLogAction.unban):
            if entry.user == guild.me:
                is_last_audit_log_me = True
            else:
                audit_moderator = entry.user
                audit_reason = entry.reason
    except Exception as e:
        try:
            if e.status == 403:
                return
        except:
            return



    if is_last_audit_log_me:
        return

    mod_log_db = MODLOG.get_server(guild.id)
    case_number = len(mod_log_db.all()) + 1

    if audit_reason == None:
        audit_reason = f"Please use `{prefix} reason {case_number} [reason]`"

    channel_to_send_to = guild.get_channel(int(DATABASE.get_server(guild.id).get(Query().modlog)['modlog']))

    embed = discord.Embed(colour=discord.Color.green())

    embed.title = f"User Unbanned"
    embed.add_field(name="User:", value=f"{user.name}#{user.discriminator}")
    embed.add_field(name="Reason:", value=f"{audit_reason}")
    embed.add_field(name="Moderator:", value=f"{audit_moderator}")
    embed.add_field(name="Case Number", value=f"{case_number}")

    message = await channel_to_send_to.send(embed=embed)

    mod_log_db.insert({
        "case": case_number,
        "reason": audit_reason,
        "userid": user.id,
        "operation": "unban",
        "moderator": audit_moderator.id,
        "message_id": message.id
    })

@CLIENT.event
async def on_message(message):
    '''On Message Event Handler for when the bot starts'''
    if isinstance(message.channel, discord.DMChannel):
        return

    if message.author.bot:
        return

    if message.channel.guild is None:
        return

    server_data = DATABASE.get_server(message.guild.id)
    try:
        prefix = server_data.get(Query().prefix)['prefix']
    except:
        server_data.insert({'prefix': settings.default_prefix})
        prefix = server_data.get(Query().prefix)['prefix']

    if len(message.content.split()) == 0:
        return

    if message.content.split()[0] == f"<@{CLIENT.user.id}>" or message.content.split()[0] == f"<@!{CLIENT.user.id}>":
        prefix = f"<@{CLIENT.user.id}>"
        pass
    elif not message.content.split()[:len(prefix.split())] == prefix.split():
        return

    if len(message.content.split()[len(prefix.split())::]) == 0:
        return

    ctx = {
        '_main': message,
        'prefix': prefix,
        'content': re.sub(" +", " ", message.content),
        'author': message.author,
        'guild': message.guild,
        'channel': message.channel,
        'command_name': re.sub(" +", " ", message.content).split()[len(prefix.split())::][0].lower(),
        'command_args': re.sub(" +", " ", message.content).split()[len(prefix.split() +
                                                       re.sub(" +", " ", message.content).split()
                                                       [len(prefix.split())::][0].split()):],
        'weebsh': sh_client,
        'modlogs': MODLOG,
        'customcoms': CUSTOMCOMMANDS
        }
    if check_permissions_channel(message.channel, message.channel.guild.get_member(CLIENT.user.id), {"send_messages": False}):
        if ctx['command_name'] in cmds:
            return await message.author.send(f"I do not have permission to talk in <#{message.channel.id}>")

    await COMMAND_REGISTERY.run_command(command_to_run=ctx['command_name'],
                                            ctx=ctx, client=CLIENT, database=DATABASE)

CLIENT.run(settings.token)
